import {
  ValidatorConstraint,
  ValidatorConstraintInterface,
  ValidationArguments,
  ValidationOptions,
  registerDecorator,
} from 'class-validator';
import ajv = require('ajv');

@ValidatorConstraint({ name: 'JSONSchema', async: false })
export class IsJSONSchemaConstraint implements ValidatorConstraintInterface {
  validate(jsonObjectOrString: {} | string, args: ValidationArguments) {
    let jsonObject =
      typeof jsonObjectOrString === 'string'
        ? JSON.parse(jsonObjectOrString)
        : jsonObjectOrString;
    if (!jsonObject) {
      // TODO: Proper error
      return false;
    }
    return ajv().validateSchema(jsonObject);
  }

  defaultMessage(args: ValidationArguments): string {
    return `${args.property} must be a JSON Schema`;
  }
}

export const IsJSONSchema = (validationOptions?: ValidationOptions) => {
  return (object: Object, propertyName: string) => {
    registerDecorator({
      target: object.constructor,
      propertyName: propertyName,
      options: validationOptions,
      constraints: [],
      validator: IsJSONSchemaConstraint,
    });
  };
};
