import { Entity, Column, Unique, PrimaryColumn } from 'typeorm';
import { DeviceType as DeviceTypeModel } from '@sensorbucket/models';

@Entity()
export class DeviceType implements DeviceTypeModel {
  @PrimaryColumn()
  id: string;

  @Column()
  name: string;

  @Column({ type: 'json' })
  configurationSchema: {};
}
