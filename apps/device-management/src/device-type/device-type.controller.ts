import {
  Controller,
  Get,
  Param,
  NotFoundException,
  Post,
  Body,
  Patch,
  Delete,
  UsePipes,
  ValidationPipe,
} from '@nestjs/common';
import { DeviceTypeService } from './device-type.service';
import { CreateDeviceTypeDTO } from './dto/createDeviceType.dto';
import { UpdateDeviceTypeDTO } from './dto/updateDeviceType.dto';

@UsePipes(new ValidationPipe({ whitelist: true }))
@Controller('device-types')
export class DeviceTypeController {
  constructor(private readonly service: DeviceTypeService) {}

  @Get()
  getMany() {
    return this.service.getMany();
  }

  @Get(':id')
  async getOne(@Param('id') id: string) {
    const devicetype = await this.service.getOne(id);
    if (!devicetype) {
      throw new NotFoundException();
    }
    return devicetype;
  }

  @Post()
  createOne(@Body() dto: CreateDeviceTypeDTO) {
    return this.service.createOne(dto);
  }

  @Patch(':id')
  async updateOne(@Param('id') id: string, @Body() dto: UpdateDeviceTypeDTO) {
    const deviceType = await this.service.getOne(id);
    if (!deviceType) {
      throw new NotFoundException();
    }
    return this.service.updateOne(deviceType, dto);
  }

  @Delete(':id')
  async deleteOne(@Param('id') id: string) {
    const devicetype = await this.service.getOne(id);
    if (!devicetype) {
      throw new NotFoundException();
    }
    return this.service.deleteOne(devicetype);
  }
}
