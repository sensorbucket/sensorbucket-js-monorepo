import { IsString, IsOptional, MinLength, MaxLength } from 'class-validator';
import { IsJSONSchema } from '../../common/jsonSchema.validator';

export class CreateDeviceTypeDTO {
  @IsString()
  @MinLength(3)
  @MaxLength(32)
  @IsOptional()
  id?: string;

  @IsString()
  @MinLength(3)
  @MaxLength(32)
  name: string;

  @IsJSONSchema()
  configurationSchema: string | { [index: string]: any };
}
