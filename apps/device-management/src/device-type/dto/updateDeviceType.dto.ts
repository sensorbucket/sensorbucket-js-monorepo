import { IsString, MinLength, MaxLength, IsOptional } from 'class-validator';
import { IsJSONSchema } from '../../common/jsonSchema.validator';

export class UpdateDeviceTypeDTO {
  @IsString()
  @MinLength(3)
  @MaxLength(32)
  @IsOptional()
  name?: string;

  @IsJSONSchema()
  @IsOptional()
  configurationSchema?: string | { [index: string]: any };
}
