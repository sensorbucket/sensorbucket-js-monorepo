import { Entity, Column, PrimaryColumn } from 'typeorm';
import { Source as SourceModel } from '@sensorbucket/models';

@Entity()
export class Source implements SourceModel {
  @PrimaryColumn()
  id: string;

  @Column()
  name: string;

  @Column({ type: 'json' })
  configurationSchema: {};
}
