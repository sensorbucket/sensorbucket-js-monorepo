import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { KeyModule } from '@sensorbucket/common';
import { SourceService } from './source.service';
import { SourceController } from './source.controller';
import { Source } from './entities/source.entity';
import { AppConfig } from '../config/config.service';

@Module({
  imports: [
    KeyModule.forRootAsync({
      inject: [AppConfig],
      useFactory: (config: AppConfig) => ({
        kmsURL: config.kmsURL,
      }),
    }),
    TypeOrmModule.forFeature([Source]),
  ],
  providers: [SourceService],
  controllers: [SourceController],
  exports: [SourceService],
})
export class SourceModule {}
