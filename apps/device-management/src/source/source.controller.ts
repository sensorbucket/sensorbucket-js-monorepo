import {
  Controller,
  Get,
  Param,
  NotFoundException,
  Post,
  Body,
  Patch,
  Delete,
  ParseIntPipe,
  BadRequestException,
} from '@nestjs/common';
import { KeyService, Token } from '@sensorbucket/common';
import { SourceService } from './source.service';
import { CreateSourceDTO } from './dto/createSource.dto';
import { UpdateSourceDTO } from './dto/updateSource.dto';

@Controller('sources')
export class SourceController {
  constructor(private readonly service: SourceService) {}

  @Get()
  getMany() {
    return this.service.getMany();
  }

  @Get(':id')
  async getOne(@Param('id') id: string) {
    const source = await this.service.getOne(id);
    if (!source) {
      throw new NotFoundException();
    }
    return source;
  }

  @Post()
  createOne(@Body() dto: CreateSourceDTO) {
    return this.service.createOne(dto);
  }

  @Patch(':id')
  async updateOne(@Param('id') id: string, @Body() dto: UpdateSourceDTO) {
    const source = await this.service.getOne(id);
    if (!source) {
      throw new NotFoundException();
    }
    return this.service.updateOne(id, dto);
  }

  @Delete(':id')
  async deleteOne(@Param('id') id: string) {
    const source = await this.service.getOne(id);
    if (!source) {
      throw new NotFoundException();
    }
    return this.service.deleteOne(id);
  }

  @Post(':id/token')
  async createToken(
    @Param('id') id: string,
    @Body('owner', ParseIntPipe) owner: number,
    @Token() userToken: any,
  ) {
    // Check if requester is part of the organisation
    const orgs = (userToken.orgIds || []) as number[];
    if (!orgs.includes(owner)) {
      throw new BadRequestException(
        'You are not a member of given organisation',
      );
    }

    const token = await this.service.createToken(id, owner);
    return {
      message: 'Created source token',
      data: token,
    };
  }
}
