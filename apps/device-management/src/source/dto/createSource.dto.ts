import {
  IsString,
  MaxLength,
  MinLength,
  IsJSON,
  IsOptional,
} from 'class-validator';
import { IsJSONSchema } from '../../common/jsonSchema.validator';

export class CreateSourceDTO {
  @IsString()
  @MaxLength(32)
  @MinLength(3)
  @IsOptional()
  id?: string;

  @IsString()
  @MaxLength(32)
  @MinLength(3)
  name: string;

  @IsJSON()
  @IsJSONSchema()
  configurationSchema: string | { [index: string]: any };
}
