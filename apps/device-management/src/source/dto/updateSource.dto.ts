import {
  IsJSON,
  IsString,
  MaxLength,
  MinLength,
  IsOptional,
} from 'class-validator';
import { IsJSONSchema } from '../../common/jsonSchema.validator';

export class UpdateSourceDTO {
  @IsString()
  @MaxLength(32)
  @MinLength(3)
  @IsOptional()
  name?: string;

  @IsJSON()
  @IsJSONSchema()
  @IsOptional()
  configurationSchema?: string | { [index: string]: any };
}
