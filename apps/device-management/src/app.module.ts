import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { LocationModule } from './location/location.module';
import { ConfigModule } from './config/config.module';
import { AppConfig } from './config/config.service';
import { SourceModule } from './source/source.module';
import { DeviceTypeModule } from './device-type/device-type.module';
import { DeviceModule } from './device/device.module';

@Module({
  imports: [
    ConfigModule,
    TypeOrmModule.forRootAsync({
      inject: [AppConfig],
      useFactory: (config: AppConfig) => ({
        type: 'postgres',
        host: config.dbHost,
        port: config.dbPort,
        username: config.dbUsername,
        password: config.dbPassword,
        database: config.dbName,
        synchronize: true,
        autoLoadEntities: true,
      }),
    }),
    LocationModule,
    DeviceTypeModule,
    DeviceModule,
    SourceModule,
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}
