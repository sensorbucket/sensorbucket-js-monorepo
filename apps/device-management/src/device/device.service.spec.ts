import { Test, TestingModule } from '@nestjs/testing';
import { DeviceService } from './device.service';
import { SourceService } from 'src/source/source.service';
import { DeviceTypeService } from 'src/device-type/device-type.service';
import { Device } from './entities/device.entity';
import { getRepositoryToken } from '@nestjs/typeorm';

const repoMock = {};

const sourceServiceMock = {};

const deviceTypeServiceMock = {};

describe('DeviceService', () => {
  let service: DeviceService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        {
          provide: getRepositoryToken(Device),
          useValue: repoMock,
        },
        {
          provide: SourceService,
          useValue: sourceServiceMock,
        },
        {
          provide: DeviceTypeService,
          useValue: deviceTypeServiceMock,
        },
        DeviceService,
      ],
    }).compile();

    service = module.get<DeviceService>(DeviceService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
