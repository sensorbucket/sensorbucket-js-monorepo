import {
  Controller,
  Get,
  Param,
  ParseIntPipe,
  NotFoundException,
  Post,
  Body,
  Patch,
  Delete,
  UsePipes,
  ValidationPipe,
  Query,
} from '@nestjs/common';
import { DeviceService } from './device.service';
import { CreateDeviceDTO } from './dto/createDevice.dto';
import { UpdateDeviceDTO } from './dto/updateDevice.dto';

@UsePipes(new ValidationPipe({ whitelist: true }))
@Controller('devices')
export class DeviceController {
  constructor(private readonly service: DeviceService) {}

  @Get()
  getMany(@Query() query: any) {
    return this.service.getMany(query);
  }

  @Get(':id')
  async getOne(@Param('id', ParseIntPipe) id: number) {
    const device = await this.service.getOne(id);
    if (!device) {
      throw new NotFoundException();
    }
    return device;
  }

  @Post()
  createOne(@Body() dto: CreateDeviceDTO) {
    return this.service.createOne(dto, 0);
  }

  @Patch(':id')
  async updateOne(
    @Param('id', ParseIntPipe) id: number,
    @Body() dto: UpdateDeviceDTO,
  ) {
    const device = await this.service.getOne(id);
    if (!device) {
      throw new NotFoundException();
    }
    return this.service.updateOne(device, dto);
  }

  @Delete(':id')
  async deleteOne(@Param('id', ParseIntPipe) id: number) {
    const device = await this.service.getOne(id);
    if (!device) {
      throw new NotFoundException();
    }
    return this.service.deleteOne(device);
  }
}
