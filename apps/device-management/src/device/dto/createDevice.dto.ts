import {
  IsObject,
  IsString,
  IsNumber,
  ValidateNested,
  MaxLength,
} from 'class-validator';
import { Type } from 'class-transformer';

class Configuration {
  @IsObject()
  source: {};

  @IsObject()
  type: {};
}

export class CreateDeviceDTO {
  @IsString()
  @MaxLength(32)
  name: string;

  @IsNumber()
  owner: number;

  @IsString()
  description: string;

  @IsString()
  source: string;

  @IsString()
  type: string;

  @ValidateNested()
  @Type(() => Configuration)
  configuration: Configuration;
}
