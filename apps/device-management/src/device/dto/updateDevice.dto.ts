import {
  IsObject,
  IsString,
  IsOptional,
  ValidateNested,
  MaxLength,
} from 'class-validator';
import { Type } from 'class-transformer';

class Configuration {
  @IsObject()
  @IsOptional()
  source?: {};

  @IsObject()
  @IsOptional()
  type?: {};
}

export class UpdateDeviceDTO {
  @IsString()
  @MaxLength(32)
  @IsOptional()
  name?: string;

  @IsString()
  @IsOptional()
  @IsOptional()
  description?: string;

  @ValidateNested()
  @Type((type) => Configuration)
  @IsOptional()
  configuration?: Configuration;
}
