import {
  PrimaryGeneratedColumn,
  Entity,
  Column,
  JoinColumn,
  ManyToOne,
} from 'typeorm';
import { Device as DeviceModel } from '@sensorbucket/models';
import { DeviceType } from '../../device-type/entities/deviceType.entity';
import { Source } from '../../source/entities/source.entity';

@Entity()
export class Device implements DeviceModel {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column()
  owner: number;

  @Column('text')
  description: string;

  @ManyToOne((type) => DeviceType)
  @JoinColumn()
  type: DeviceType;

  @Column({ nullable: true })
  typeId: string;

  @ManyToOne((type) => Source)
  @JoinColumn()
  source: Source;

  @Column({ nullable: true })
  sourceId: string;

  @Column('json')
  configuration: {};
}
