import { Injectable, BadRequestException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import * as Ajv from 'ajv';
import { keys } from 'lodash';
import { Device } from './entities/device.entity';
import { CreateDeviceDTO } from './dto/createDevice.dto';
import { UpdateDeviceDTO } from './dto/updateDevice.dto';
import { SourceService } from '../source/source.service';
import { DeviceTypeService } from '../device-type/device-type.service';
import { Source } from '../source/entities/source.entity';
import { DeviceType } from '../device-type/entities/deviceType.entity';

interface Configuration {
  source: {};
  type: {};
}

@Injectable()
export class DeviceService {
  private readonly allowedFilters = {
    sourceId: {},
    typeId: {},
    owner: {},
    configuration: { json: true },
  };

  constructor(
    @InjectRepository(Device) private readonly repo: Repository<Device>,
    private readonly sourceService: SourceService,
    private readonly deviceTypeService: DeviceTypeService,
  ) {}

  /**
   *
   * @param configuration
   * @param source
   * @param deviceType
   */
  private validateConfiguration(
    configuration: Configuration,
    source: Source,
    deviceType: DeviceType,
  ): string[] {
    let errors = [];
    const ajv = new Ajv();

    const schema = {
      additionalProperties: false,
      type: 'object',
      required: ['source', 'type'],
      properties: {
        source: source.configurationSchema,
        type: deviceType.configurationSchema,
      },
    };

    // Validate Source Configuration
    ajv.validate(schema, configuration);
    if (ajv.errors?.length) {
      errors.push(ajv.errorsText(ajv.errors, { dataVar: 'configuration' }));
    }

    return errors;
  }

  /**
   * Get all devices from joined organisations
   */
  async getMany(filter?: Record<string, any>): Promise<Device[]> {
    let query = this.repo.createQueryBuilder().select();

    // Apply filters
    if (filter) {
      // Loop properties and values
      for (const property in filter) {
        // If the property is not allowed skip it
        if (!keys(this.allowedFilters).includes(property)) {
          continue;
        }

        // Ensure it's not a prototype property
        if (filter.hasOwnProperty(property)) {
          const value = filter[property];
          let comparator = '=';

          // If the column is a json column then use a different comparator
          if (this.allowedFilters[property].json === true) {
            comparator = '::jsonb @>';
          }

          /* Build where query, notice that property is also used as parameter key
            if we were to use :value for parameter key then all filters/where clauses will try to match
            against the last set value for :value!
            
            Example:
              let property = 'owner';
              let comparator = '=';
              let value = 5;
              
              query = query.andWhere(`"owner" = :owner`, {
                owner: 5,
              });

          */
          query = query.andWhere(`"${property}" ${comparator} :${property}`, {
            [property]: value,
          });
        }
      }
    }

    return query.getMany();
  }

  /**
   *
   * @param id
   */
  async getOne(id: number): Promise<Device> {
    return this.repo
      .createQueryBuilder()
      .select()
      .where('id = :id', { id })
      .getOne();
  }

  /**
   *
   * @param dto
   * @param user
   */
  async createOne(dto: CreateDeviceDTO, user: number): Promise<Device> {
    // Source and DeviceType must exist
    const [source, type] = await Promise.all([
      this.sourceService.getOne(dto.source),
      this.deviceTypeService.getOne(dto.type),
    ]);
    if (!source) {
      throw new BadRequestException('Source does not exist');
    }
    if (!type) {
      throw new BadRequestException('DeviceType does not exist');
    }

    // Validate configuration
    const errors = this.validateConfiguration(dto.configuration, source, type);
    if (errors.length > 0) {
      throw new BadRequestException(errors);
    }

    const result = await this.repo
      .createQueryBuilder()
      .insert()
      .values([
        {
          ...dto,
          source: source,
          type: type,
        },
      ])
      .returning('*')
      .execute();
    return this.getOne(result.generatedMaps[0].id);
  }

  /**
   *
   * @param device
   * @param dto
   */
  async updateOne(device: Device, dto: UpdateDeviceDTO): Promise<Device> {
    if (typeof device.id !== 'number') {
      throw new Error('Cannot update device without id!');
    }
    await this.repo
      .createQueryBuilder()
      .update()
      .set(dto as {})
      .where('id = :id', { id: device.id })
      .execute();
    return this.getOne(device.id);
  }

  /**
   *
   * @param device
   */
  async deleteOne(device: Device): Promise<void> {
    if (typeof device.id !== 'number') {
      throw new Error('Cannot delete device without id!');
    }
    const result = await this.repo
      .createQueryBuilder()
      .delete()
      .where('id = :id', { id: device.id })
      .execute();
  }
}
