import { Module } from '@nestjs/common';
import { DeviceService } from './device.service';
import { DeviceController } from './device.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Device } from './entities/device.entity';
import { SourceModule } from '../source/source.module';
import { DeviceTypeModule } from '../device-type/device-type.module';

@Module({
  imports: [TypeOrmModule.forFeature([Device]), SourceModule, DeviceTypeModule],
  providers: [DeviceService],
  controllers: [DeviceController],
})
export class DeviceModule {}
