import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { AuthenticationMiddleware } from '@sensorbucket/common';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.use(AuthenticationMiddleware);
  await app.listen(3000);
}
bootstrap();
