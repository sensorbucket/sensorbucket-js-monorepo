import { Injectable } from '@nestjs/common';
import { Repository } from 'typeorm';
import { Location } from './entities/location.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { CreateLocationDTO } from './dto/createLocation.dto';
import { UpdateLocationDTO } from './dto/updateLocation.dto';

@Injectable()
export class LocationService {
  constructor(
    @InjectRepository(Location) private readonly repo: Repository<Location>,
  ) {}

  /**
   * Get all locations from joined organisations
   */
  async getMany(): Promise<Location[]> {
    return this.repo.createQueryBuilder().select().getMany();
  }

  async getOne(id: number): Promise<Location> {
    return this.repo
      .createQueryBuilder()
      .select()
      .where('id = :id', { id })
      .getOne();
  }

  async createOne(dto: CreateLocationDTO, user: number): Promise<Location> {
    const result = await this.repo
      .createQueryBuilder()
      .insert()
      .values([dto as {}])
      .returning('*')
      .execute();
    return this.getOne(result.generatedMaps[0].id);
  }

  async updateOne(
    location: Location,
    dto: UpdateLocationDTO,
  ): Promise<Location> {
    if (typeof location.id !== 'number') {
      throw new Error('Cannot update location without id!');
    }
    await this.repo
      .createQueryBuilder()
      .update()
      .set(dto as {})
      .where('id = :id', { id: location.id })
      .execute();
    return this.getOne(location.id);
  }

  async deleteOne(location: Location): Promise<void> {
    if (typeof location.id !== 'number') {
      throw new Error('Cannot delete location without id!');
    }
    const result = await this.repo
      .createQueryBuilder()
      .delete()
      .where('id = :id', { id: location.id })
      .execute();
  }
}
