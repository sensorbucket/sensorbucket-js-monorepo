import {
  Controller,
  Get,
  Param,
  ParseIntPipe,
  Post,
  Body,
  UsePipes,
  ValidationPipe,
  Patch,
  Delete,
  NotFoundException,
} from '@nestjs/common';
import { LocationService } from './location.service';
import { CreateLocationDTO } from './dto/createLocation.dto';
import { UpdateLocationDTO } from './dto/updateLocation.dto';

@UsePipes(new ValidationPipe({ whitelist: true }))
@Controller('locations')
export class LocationController {
  constructor(private readonly service: LocationService) {}

  @Get()
  getMany() {
    return this.service.getMany();
  }

  @Get(':id')
  async getOne(@Param('id', ParseIntPipe) id: number) {
    const location = await this.service.getOne(id);
    if (!location) {
      throw new NotFoundException();
    }
    return location;
  }

  @Post()
  createOne(@Body() dto: CreateLocationDTO) {
    return this.service.createOne(dto, 0);
  }

  @Patch(':id')
  async updateOne(
    @Param('id', ParseIntPipe) id: number,
    @Body() dto: UpdateLocationDTO,
  ) {
    const location = await this.service.getOne(id);
    if (!location) {
      throw new NotFoundException();
    }
    return this.service.updateOne(location, dto);
  }

  @Delete(':id')
  async deleteOne(@Param('id', ParseIntPipe) id: number) {
    const location = await this.service.getOne(id);
    if (!location) {
      throw new NotFoundException();
    }
    return this.service.deleteOne(location);
  }
}
