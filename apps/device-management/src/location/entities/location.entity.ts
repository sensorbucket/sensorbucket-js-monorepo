import { Entity, PrimaryGeneratedColumn, Column } from 'typeorm';
import { Location as LocationModel } from '@sensorbucket/models';

@Entity()
export class Location implements LocationModel {
  @PrimaryGeneratedColumn()
  id: number;

  // External reference to Organisation
  @Column()
  owner: number;

  @Column({ length: 75 })
  name: string;

  @Column({ type: 'text' })
  description: string;

  @Column({
    type: 'geometry',
    srid: 4326,
  })
  geom: any;
}
