import {
  IsNumber,
  IsOptional,
  IsString,
  MaxLength,
  ValidateNested,
} from 'class-validator';
import { GeoJSON } from './geoJson';
import { Type } from 'class-transformer';

export class UpdateLocationDTO {
  @IsNumber()
  @IsOptional()
  owner: number;

  @IsString()
  @MaxLength(75)
  @IsOptional()
  name: string;

  @IsString()
  @MaxLength(255)
  @IsOptional()
  description: string;

  @ValidateNested()
  @Type(() => GeoJSON)
  @IsOptional()
  geom: GeoJSON;
}
