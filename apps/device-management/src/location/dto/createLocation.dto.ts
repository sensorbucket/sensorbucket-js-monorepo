import { IsNumber, IsString, MaxLength, ValidateNested } from 'class-validator';
import { Type } from 'class-transformer';
import { GeoJSON } from './geoJson';

/**
 * DTO for creating a new location
 */
export class CreateLocationDTO {
  @IsString()
  @MaxLength(75)
  name: string;

  @IsString()
  @MaxLength(255)
  description: string;

  @IsNumber()
  owner: number;

  @ValidateNested()
  @Type(() => GeoJSON)
  geom: GeoJSON;
}
