import { Injectable, Logger } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';

@Injectable()
export class AppConfig {
  constructor(private readonly config: ConfigService) {}

  //
  //  Database configuration
  //

  get dbHost() {
    return this.config.get('DB_HOST', '127.0.0.1');
  }

  get dbPort() {
    return parseInt(this.config.get<string>('DB_PORT', '5432'));
  }

  get dbUsername() {
    return this.config.get('DB_USERNAME', 'root');
  }

  get dbPassword() {
    return this.config.get('DB_PASSWORD', 'root');
  }

  get dbName() {
    return this.config.get('DB_NAME', 'sensorbucket');
  }

  //
  // Token generation
  //

  get jwtExpiresIn() {
    return this.config.get('JWT_EXPIRES_IN', '15m');
  }

  get kmsURL() {
    return this.config.get('KMS_URL', 'http://key-management');
  }

  get kmsKeystore() {
    return this.config.get('KMS_KEYSTORE', 'device-management');
  }
}
