import { Test, TestingModule } from '@nestjs/testing';
import { ConfigService } from '@nestjs/config';
import * as request from 'supertest';
import { AppModule } from 'src/app.module';
import { ConfigFixture, cleanDatabase, loadFixtures } from './fixtures';
import { GenericCrudSpecE2E } from './genericCrudSpec';
import { CreateSourceDTO } from 'src/source/dto/createSource.dto';
import { UpdateSourceDTO } from 'src/source/dto/updateSource.dto';

describe('Sources (e2e)', () => {
  let app;
  let agent: request.SuperTest<request.Test>;

  //
  // Fixtures
  //
  const sourceDTO: CreateSourceDTO = {
    id: 'newlocation',
    name: 'NewLocation',
    configurationSchema: {
      $schema: 'http://json-schema.org/draft-07/schema#',
      type: 'object',
      required: ['apples'],
      properties: {
        apples: {
          type: 'number',
          minimum: 5,
        },
      },
    },
  };
  const updateSourceDTO: UpdateSourceDTO = {
    name: 'updatedName',
  };

  //
  // Tests
  //

  beforeAll(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    })
      .overrideProvider(ConfigService)
      .useValue(ConfigFixture)
      .compile();

    app = moduleFixture.createNestApplication();
    await app.init();

    await loadFixtures(__dirname + '/fixtures');

    agent = request.agent(app.getHttpServer());
  });

  afterAll(async () => {
    await cleanDatabase();
    await app.close();
  });
  /**
   *
   */
  it(`Should create new Source`, async () => {
    return agent
      .post('/sources')
      .send(sourceDTO)
      .expect(201)
      .expect((res) => {
        expect(res.body).toMatchObject(sourceDTO);
        expect(res.body).toHaveProperty('id', sourceDTO.id);
      });
  });

  /**
   *
   */
  it(`Get return created Source`, async () => {
    return agent
      .get(`/sources/${sourceDTO.id}`)
      .send()
      .expect(200)
      .expect((res) => {
        expect(res.body).toMatchObject(sourceDTO);
      });
  });

  /**
   *
   */
  describe(`Should patch Source`, () => {
    it(`update Source`, async () => {
      return agent
        .patch('/sources/ttn')
        .send(updateSourceDTO)
        .expect(200)
        .expect((res) => {
          expect(res.body).toMatchObject(updateSourceDTO);
        });
    });
    it(`get and verify updated`, async () => {
      return agent
        .get('/sources/ttn')
        .send()
        .expect(200)
        .expect((res) => {
          expect(res.body).toMatchObject(updateSourceDTO);
        });
    });
  });

  /**
   *
   */
  it.skip(`Get Source owned by joined organisations`, async () => {});

  /**
   *
   */
  describe(`Should delete Source`, () => {
    it(`delete Source`, async () => {
      return agent.delete(`/sources/${sourceDTO.id}`).send().expect(200);
    });
    it(`expect 404`, async () => {
      return agent.get(`/sources/${sourceDTO.id}`).send().expect(404);
    });
  });

  it(`Should throw NotFound on non-existing Source`, async () => {
    return agent.get(`/sources/99999`).send().expect(404);
  });

  it('Create should throw BadRequest on invalid configuration schema', () => {
    return agent
      .post('/sources')
      .send({
        name: 'InvalidSource',
        configurationSchema: {
          type: 'Abject', // intentional misspelling
        },
      })
      .expect(400);
  });
});
