import { Test, TestingModule } from '@nestjs/testing';
import { ConfigService } from '@nestjs/config';
import * as request from 'supertest';
import { AppModule } from 'src/app.module';
import { ConfigFixture, cleanDatabase, loadFixtures } from './fixtures';
import { CreateLocationDTO } from 'src/location/dto/createLocation.dto';
import { GenericCrudSpecE2E } from './genericCrudSpec';

describe('Locations (e2e)', () => {
  let app;
  let agent: request.SuperTest<request.Test>;

  //
  // Fixtures
  //
  const locationDTO: CreateLocationDTO = {
    name: 'NewLocation',
    description: 'A new location created for testing purposes',
    owner: 0,
    geom: {
      type: 'Point',
      coordinates: [0, 0],
    },
  };

  //
  // Tests
  //

  beforeAll(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    })
      .overrideProvider(ConfigService)
      .useValue(ConfigFixture)
      .compile();

    app = moduleFixture.createNestApplication();
    await app.init();

    await loadFixtures(__dirname + '/fixtures');

    agent = request.agent(app.getHttpServer());
  });

  afterAll(async () => {
    await cleanDatabase();
    await app.close();
  });

  /**
   * Apply generic e2e CRUD tests
   */
  GenericCrudSpecE2E({
    getAgent: () => agent,
    entityName: 'Location',
    baseUrl: '/locations',
    createDTO: locationDTO,
    patchDTO: {
      description: 'This description is patched!',
    },
  })();

  /**
   * 'geom' Property is a GeoJSON type
   * and has to be of type 'Point'
   * with two coordinates
   */
  describe('Should throw 400 on incorrect geometry', () => {
    const incorrectGeom = [
      [
        'three coordinates',
        {
          type: 'Point',
          coordinates: [0, 0, 0],
        },
      ],
      [
        'one coordinates',
        {
          type: 'Point',
          coordinates: [0],
        },
      ],
      [
        'missing coordinates',
        {
          type: 'Point',
        },
      ],
      [
        'missing type',
        {
          coordinates: [0, 0],
        },
      ],
      [
        'different `type`',
        {
          type: 'Polygon',
          coordinates: [0, 0],
        },
      ],
    ];
    it.each(incorrectGeom)('on create with %s', async (_: any, geom: any) => {
      return agent
        .post('/locations')
        .send({ ...locationDTO, geom })
        .expect(400);
    });
    it.each(incorrectGeom)('on patch with %s', async (_: any, geom: any) => {
      return agent.patch('/locations/1').send({ geom }).expect(400);
    });
  });
});
