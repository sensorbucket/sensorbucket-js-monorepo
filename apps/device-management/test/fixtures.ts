import { Connection, getConnection, getRepository } from 'typeorm';
import {
  Loader,
  Builder,
  Parser,
  fixturesIterator,
  Resolver,
} from 'typeorm-fixtures-cli/dist';
import path = require('path');

const config = {
  DB_HOST: '127.0.0.1',
  DB_PORT: 5432,
  DB_USERNAME: 'root',
  DB_PASSWORD: 'root',
  DB_NAME: 'sensorbucket-test',
  JWT_SECRET: 'defaultsecret',
  JWT_EXPIRY: '6h',
  JWT_USER_EXPIRY: '6h',
};

export const ConfigFixture = {
  get<T>(key: string, fallback?: T): T {
    const value = config[key] as T;
    if (value === undefined) {
      return fallback;
    }
    return value;
  },
};

export const loadFixtures = async (fixturesPath: string) => {
  let connection;

  try {
    connection = getConnection();
    await connection.synchronize(true);

    const loader = new Loader();
    loader.load(path.resolve(fixturesPath));

    const resolver = new Resolver();
    const fixtures = resolver.resolve(loader.fixtureConfigs);
    const builder = new Builder(connection, new Parser());

    for (const fixture of fixturesIterator(fixtures)) {
      const entity = await builder.build(fixture);
      await getRepository(entity.constructor.name).save(entity);
    }
  } catch (err) {
    throw err;
  }
};

/**
 * Cleans all the entities
 */
const cleanAll = async (connection: Connection, entities: any[]) => {
  try {
    const tables = entities.map(({ tableName }) => `"${tableName}"`).join(', ');
    debugger;
    await connection.query(`TRUNCATE TABLE ${tables};`);
  } catch (error) {
    throw new Error(`ERROR: Cleaning test db: ${error}`);
  }
};

/**
 * Returns the entites of the database
 */
const getEntities = (connection: Connection) => {
  const entities = [];
  connection.entityMetadatas.forEach(({ name, tableName }) =>
    entities.push({ name, tableName }),
  );
  return entities;
};

export const cleanDatabase = async () => {
  const connection = getConnection();
  const entities = getEntities(connection);
  return cleanAll(connection, entities);
};
