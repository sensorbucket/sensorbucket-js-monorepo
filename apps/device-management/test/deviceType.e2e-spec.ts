import { Test, TestingModule } from '@nestjs/testing';
import { ConfigService } from '@nestjs/config';
import * as request from 'supertest';
import { AppModule } from 'src/app.module';
import { ConfigFixture, cleanDatabase, loadFixtures } from './fixtures';
import { CreateDeviceTypeDTO } from 'src/device-type/dto/createDeviceType.dto';
import { UpdateDeviceTypeDTO } from 'src/device-type/dto/updateDeviceType.dto';

describe('DeviceType (e2e)', () => {
  let app;
  let agent: request.SuperTest<request.Test>;

  //
  // Fixtures
  //
  const deviceTypeDTO: CreateDeviceTypeDTO = {
    id: 'newdevicetype',
    name: 'NewDeviceType',
    configurationSchema: {
      $schema: 'http://json-schema.org/draft-07/schema#',
      type: 'object',
      required: ['apples'],
      properties: {
        apples: {
          type: 'number',
          minimum: 5,
        },
      },
    },
  };
  const updateDeviceTypeDTO: UpdateDeviceTypeDTO = {
    configurationSchema: {
      $schema: 'http://json-schema.org/draft-07/schema#',
      type: 'object',
      required: ['apples'],
      properties: {
        apples: {
          type: 'number',
          minimum: 0,
        },
      },
    },
  };

  //
  // Tests
  //

  beforeAll(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    })
      .overrideProvider(ConfigService)
      .useValue(ConfigFixture)
      .compile();

    app = moduleFixture.createNestApplication();
    await app.init();

    await loadFixtures(__dirname + '/fixtures');

    agent = request.agent(app.getHttpServer());
  });

  afterAll(async () => {
    await cleanDatabase();
    await app.close();
  });

  /**
   *
   */
  it('Should create new DeviceType', async () => {
    return agent
      .post('/device-types')
      .send(deviceTypeDTO)
      .expect(201)
      .expect((res) => {
        expect(res.body).toMatchObject(deviceTypeDTO);
      });
  });

  /**
   *
   */
  it('Get return created DeviceType', async () => {
    const { id } = deviceTypeDTO;
    return agent
      .get(`/device-types/${id}`)
      .send()
      .expect(200)
      .expect((res) => {
        expect(res.body).toMatchObject(deviceTypeDTO);
      });
  });

  /**
   *
   */
  describe('Should patch DeviceType', () => {
    it('update DeviceType', async () => {
      return agent
        .patch('/device-types/first-device-type')
        .send(updateDeviceTypeDTO)
        .expect(200)
        .expect((res) => {
          expect(res.body).toMatchObject(updateDeviceTypeDTO);
        });
    });
    it(`get and verify updated`, async () => {
      return agent
        .get('/device-types/first-device-type')
        .send()
        .expect(200)
        .expect((res) => {
          expect(res.body).toMatchObject(updateDeviceTypeDTO);
        });
    });
  });

  /**
   *
   */
  it.skip(`Get DeviceType'owned by joined organisations`, async () => {});

  /**
   *
   */
  describe('Should delete DeviceType', () => {
    const { id } = deviceTypeDTO;
    it('delete DeviceType', async () => {
      return agent.delete(`/device-types/${id}`).send().expect(200);
    });
    it(`expect 404`, async () => {
      return agent.get(`/device-types/${id}`).send().expect(404);
    });
  });

  it('GetManyVersions Should throw NotFound on non-existing DeviceType id', async () => {
    return agent.get('/device-types/nonexisting').send().expect(404);
  });

  it('Create should throw BadRequest on invalid configuration schema', () => {
    return agent
      .post('/device-types')
      .send({
        name: 'InvalidDeviceType',
        configurationSchema: {
          type: 'Abject', // intentional misspelling
        },
      })
      .expect(400);
  });

  it('Create should throw BadRequest on duplicate name', () => {
    return agent
      .post('/device-types')
      .send({
        id: 'first-device-type',
        name: 'FirstDeviceType',
        configurationSchema: {
          type: 'object',
        },
      })
      .expect(400);
  });
});
