import { Module } from '@nestjs/common';
import { UserModule } from './user/user.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AuthenticationModule } from './authentication/authentication.module';
import { OrganisationModule } from './organisation/organisation.module';
import { AppConfig } from './config/config.service';
import { ConfigModule } from './config/config.module';

@Module({
  imports: [
    ConfigModule,
    TypeOrmModule.forRootAsync({
      imports: [],
      inject: [AppConfig],
      useFactory: (config: AppConfig) => ({
        type: 'postgres',
        host: config.dbHost,
        port: config.dbPort,
        username: config.dbUsername,
        password: config.dbPassword,
        database: config.dbName,
        synchronize: true,
        autoLoadEntities: true,
      }),
    }),
    UserModule,
    AuthenticationModule,
    OrganisationModule,
  ],
  controllers: [],
  providers: [],
  exports: [],
})
export class AppModule {}
