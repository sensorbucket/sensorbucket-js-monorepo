import { Injectable, Logger } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';

@Injectable()
export class AppConfig {
  constructor(private readonly config: ConfigService) {}

  //
  //  Database configuration
  //

  get dbHost() {
    return this.config.get('DB_HOST', '127.0.0.1');
  }

  get dbPort() {
    return parseInt(this.config.get<string>('DB_PORT', '5432'));
  }

  get dbUsername() {
    return this.config.get('DB_USERNAME', 'root');
  }

  get dbPassword() {
    return this.config.get('DB_PASSWORD', 'root');
  }

  get dbName() {
    return this.config.get('DB_NAME', 'sensorbucket');
  }

  get kmsURL() {
    return this.config.get('KMS_URL', 'http://sb-key-management');
  }

  get kmsKeystore() {
    return this.config.get('KMS_KEYSTORE', 'users');
  }
}
