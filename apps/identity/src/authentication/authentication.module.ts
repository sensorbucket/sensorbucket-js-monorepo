import { Module } from '@nestjs/common';
import { AuthenticationService } from './authentication.service';
import { AuthenticationController } from './authentication.controller';
import { UserModule } from '../user/user.module';
import { LocalStrategy } from './local.strategy';
import { JWTStrategy } from './jwt.strategy';
import { OrganisationModule } from '../organisation/organisation.module';
import { TokenModule } from '../token/token.module';

@Module({
  imports: [UserModule, OrganisationModule, TokenModule],
  providers: [AuthenticationService, LocalStrategy, JWTStrategy],
  controllers: [AuthenticationController],
})
export class AuthenticationModule {}
