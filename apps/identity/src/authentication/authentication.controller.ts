import {
  Controller,
  Post,
  Req,
  UseGuards,
  UsePipes,
  ValidationPipe,
  Delete,
  Body,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { TokenService } from '../token/token.service';
import { RevokeTokenDTO } from './dto/revokeToken.dto';
import { AuthenticationService } from './authentication.service';

@Controller('auth')
@UsePipes(new ValidationPipe({ whitelist: true }))
export class AuthenticationController {
  constructor(
    private readonly service: AuthenticationService,
    private readonly tokenService: TokenService,
  ) {}

  @UseGuards(AuthGuard('local'))
  @Post()
  async authenticateUser(@Req() req: any) {
    return {
      token: await this.service.createTokenForUser(req.user),
    };
  }

  @UseGuards(AuthGuard('sensorbucket-jwt'))
  @Post('token')
  authenticateToken(@Req() req: any) {
    return req.user;
  }

  @UseGuards(AuthGuard('sensorbucket-jwt'))
  @Delete('token')
  revokeToken(@Body() dto: RevokeTokenDTO) {
    this.tokenService.revoke(dto.token);
  }
}
