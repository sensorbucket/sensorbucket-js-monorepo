import { IsJWT } from 'class-validator';

export class RevokeTokenDTO {
  @IsJWT()
  token: string;
}
