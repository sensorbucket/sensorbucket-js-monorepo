import { IsString } from 'class-validator';

export class AuthenticateUserDTO {
  @IsString()
  email: string;

  @IsString()
  password: string;
}
