import { Test, TestingModule } from '@nestjs/testing';
import { AuthenticationService } from './authentication.service';
import { UserService } from '../user/user.service';
import { User } from '../user/entities/user.entity';
import { UnauthorizedException } from '@nestjs/common';
import { ConfigModule } from '../config/config.module';
import { TokenService } from '../token/token.service';
import { OrganisationService } from '../organisation/organisation.service';
import { Organisation } from '../organisation/entities/organisation.entity';

const UserServiceMock = {
  getUserById: jest.fn(),
  getUserByEmail: jest.fn(),
};

const UserTokenBuilder = {
  withOrganisations: jest.fn(() => UserTokenBuilder),
  build: jest.fn(),
};

const TokenServiceMock = {
  createUserTokenBuilder: jest.fn(() => UserTokenBuilder),
};

const OrganisationServiceMock = {
  getManyForUser: jest.fn(),
};

const dbUser: User = {
  id: 1,
  email: 'john@sensorbucket.nl',
  firstName: 'John',
  middleName: '',
  lastName: 'Doe',
  // 'difficultpassword' bcrypt 10 rounds
  passwordHash: '$2b$10$MKdr4gTQtwlNetijF5VP1eUT12mcBD4krkQkQ96oQeI0wUmLCrtea',
  organisations: [],
};

describe('AuthenticationService', () => {
  let service: AuthenticationService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [ConfigModule],
      providers: [
        // Mock UserService
        {
          provide: UserService,
          useValue: UserServiceMock,
        },
        // Mock TokenService
        {
          provide: TokenService,
          useValue: TokenServiceMock,
        },
        // Mock OrganisationService
        {
          provide: OrganisationService,
          useValue: OrganisationServiceMock,
        },
        AuthenticationService,
      ],
    }).compile();

    service = module.get<AuthenticationService>(AuthenticationService);
  });

  afterEach(() => {
    UserServiceMock.getUserByEmail.mockClear();
  });

  describe('#authenticateUser', () => {
    it('Should return user', async () => {
      // Arrange
      const email = dbUser.email;
      const password = 'difficultpassword';

      // Mock implementation
      UserServiceMock.getUserByEmail.mockResolvedValue(dbUser);

      // Act
      const user = await service.authenticateUser(email, password);

      // Assert
      expect(UserServiceMock.getUserByEmail.mock.calls[0][0]).toEqual(email);
      expect(user).toEqual(dbUser);
    });

    it('Should throw UnauthorizedError if user does not exist', async () => {
      // Arrange
      const email = 'idontexist@sensorbucket.nl';
      const password = 'asdasd';

      // Mock implementation
      UserServiceMock.getUserByEmail.mockResolvedValue(null);

      // Act
      const promise = service.authenticateUser(email, password);

      // Assert
      await expect(promise).rejects.toBeInstanceOf(UnauthorizedException);
    });

    it('Should throw UnauthorizedError if password is incorrect', async () => {
      // Arrange
      const email = dbUser.email;
      const wrongPassword = 'wrongpassword';

      // Mock implementation
      UserServiceMock.getUserByEmail.mockResolvedValue(dbUser);

      // Act
      const promise = service.authenticateUser(email, wrongPassword);

      // Assert
      await expect(promise).rejects.toBeInstanceOf(UnauthorizedException);
    });
  });

  describe('#createTokenForUser', () => {
    const dbOrgA: Organisation = {
      id: 0,
      name: 'Organisation A',
      address: 'address',
      city: 'city',
      geom: {
        type: 'Point',
        coordinates: [0, 0],
      },
      members: [],
      zipcode: '1234AB',
    };
    const dbOrgB: Organisation = {
      ...dbOrgA,
      name: 'Organisation B',
    };

    it('Should fetch and include organisations in user token', async () => {
      const orgs = [dbOrgA, dbOrgB];
      const orgIds = orgs.map((org) => org.id);
      OrganisationServiceMock.getManyForUser.mockResolvedValue(orgs);
      UserServiceMock.getUserById.mockResolvedValue(dbUser);

      //
      await service.createTokenForUser(dbUser);

      //
      expect(OrganisationServiceMock.getManyForUser).toBeCalledWith(dbUser);
      expect(UserTokenBuilder.withOrganisations).toBeCalledWith(orgIds);
    });
  });
});
