import { Injectable, UnauthorizedException } from '@nestjs/common';
import * as bcrypt from 'bcrypt';
import { UserService } from '../user/user.service';
import { User } from '../user/entities/user.entity';
import { OrganisationService } from '../organisation/organisation.service';
import { TokenService } from '../token/token.service';

@Injectable()
export class AuthenticationService {
  constructor(
    private readonly userService: UserService,
    private readonly orgService: OrganisationService,
    private readonly tokenService: TokenService,
  ) {}

  private async comparePassword(
    password: string,
    hash: string,
  ): Promise<boolean> {
    return bcrypt.compare(password, hash);
  }

  /**
   * Authenticate a user by email and password
   * @param email The user's email
   * @param password The user's password
   */
  async authenticateUser(email: string, password: string): Promise<User> {
    // Find user
    const user = await this.userService.getUserByEmail(email, {
      addSelect: ['user.passwordHash'],
    });

    // User is null if it doesn't exist
    if (!user) {
      throw new UnauthorizedException();
    }
    // Compare password to hash
    const valid = await this.comparePassword(password, user.passwordHash);
    // Throw if password is incorrect
    if (!valid) {
      throw new UnauthorizedException();
    }
    return user;
  }

  async createTokenForUser(user: User): Promise<string> {
    // Get user and its organisations
    const userOrganisations = await this.orgService.getManyForUser(user);
    const orgIds = userOrganisations.map((org) => org.id);

    // TODO: Build the token
    return this.tokenService.createUserToken(user.id, orgIds);
  }
}
