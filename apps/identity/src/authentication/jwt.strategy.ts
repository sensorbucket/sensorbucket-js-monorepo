import {
  Injectable,
  UnauthorizedException,
  InternalServerErrorException,
  Logger,
} from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { Strategy as CustomStrategy } from 'passport-strategy';
import * as JWT from 'jsonwebtoken';

const logger = new Logger('JWTStrategy', true);

class Strategy extends CustomStrategy {
  name = 'sensorbucket-jwt';
  authenticate(req: any, options?: any) {
    // Must have "Authorization: Bearer ..." header
    const authHeader =
      req.headers['authorization'] || req.headers['Authorization'];
    if (typeof authHeader !== 'string' || !authHeader.startsWith('Bearer')) {
      logger.log(
        'Authentication failed because no "Authorization: Bearer ..." header was found.',
      );
      return this.fail(401);
    }

    // JWT must exist of three parts
    const jwtStr = authHeader.slice('Bearer '.length);

    try {
      const jwt = JWT.decode(jwtStr);

      // Fail
      if (jwt === null) {
        throw new Error();
      }

      return this.success(jwt.sub, jwt);
    } catch (e) {
      logger.log('Authentication failed because JWT could not be decoded.');
      return this.fail(401);
    }
  }
}

@Injectable()
export class JWTStrategy extends PassportStrategy(Strategy) {}
