import {
  Equals,
  IsNumber,
  ArrayMinSize,
  ArrayMaxSize,
  IsString,
} from 'class-validator';

export class Geometry {
  @IsString()
  @Equals('Point')
  type: string;

  @IsNumber({}, { each: true })
  @ArrayMinSize(2)
  @ArrayMaxSize(2)
  coordinates: number[];
}
