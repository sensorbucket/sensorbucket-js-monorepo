import { CanActivate, ExecutionContext } from '@nestjs/common';
import { Observable } from 'rxjs';

type CanActivateResult = boolean | Promise<boolean> | Observable<boolean>;

interface GuardOptions {
  parameterName?: string;
}

// The wrapper
export const OnlySelfGuard = ({ parameterName }: GuardOptions) => {
  // The guard class
  return class OnlySelfGuard implements CanActivate {
    /**
     * Extracts the UserId from a given field
     * This field can be: a parameter
     * @param context The current context
     */
    getUserId(context: ExecutionContext): number {
      const http = context.switchToHttp();
      const req = http.getRequest();
      // Use the first specified option
      if (typeof parameterName === 'string') {
        return parseInt(req.params[parameterName]);
      }
    }

    canActivate(context: ExecutionContext): CanActivateResult {
      const http = context.switchToHttp();
      const req = http.getRequest();
      const userId = this.getUserId(context);
      // Get context
      if (req.user.sub === userId) {
        return true;
      }
      return false;
    }
  };
};
