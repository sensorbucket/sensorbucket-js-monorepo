export const objectIsEmpty = (obj): Boolean => {
  return !(Object.keys(obj).length > 0);
};
