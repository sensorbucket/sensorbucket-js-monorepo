import { IsString, ValidateNested } from 'class-validator';
import { Type } from 'class-transformer';
import { Geometry } from '../../common/geometry.dto';

export class CreateOrganisationDTO {
  @IsString()
  name: string;

  @IsString()
  address: string;

  @IsString()
  zipcode: string;

  @IsString()
  city: string;

  @ValidateNested()
  @Type(() => Geometry)
  geom: Geometry;
}
