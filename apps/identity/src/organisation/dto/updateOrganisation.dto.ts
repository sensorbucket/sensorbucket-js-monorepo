import { IsString, IsOptional, ValidateNested } from 'class-validator';
import { Geometry } from '../../common/geometry.dto';

export class UpdateOrganisationDTO {
  @IsString()
  @IsOptional()
  address: string;

  @IsString()
  @IsOptional()
  zipcode: string;

  @IsString()
  @IsOptional()
  city: string;

  @ValidateNested()
  @IsOptional()
  geom: Geometry;
}
