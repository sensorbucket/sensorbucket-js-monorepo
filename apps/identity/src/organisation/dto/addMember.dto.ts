import { IsNumber, IsOptional } from 'class-validator';

export class AddMemberDTO {
  @IsNumber({
    maxDecimalPlaces: 0,
  })
  @IsOptional()
  userId: number;

  @IsNumber({}, { each: true })
  @IsOptional()
  userIds: number[];
}
