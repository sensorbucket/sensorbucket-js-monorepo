import { Module } from '@nestjs/common';
import { OrganisationService } from './organisation.service';
import { OrganisationController } from './organisation.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Organisation } from './entities/organisation.entity';
import { UserController } from './user.controller';
import { UserModule } from '../user/user.module';

@Module({
  imports: [TypeOrmModule.forFeature([Organisation]), UserModule],
  providers: [OrganisationService],
  controllers: [OrganisationController, UserController],
  exports: [OrganisationService],
})
export class OrganisationModule {}
