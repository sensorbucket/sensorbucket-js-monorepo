import {
  Controller,
  Get,
  Param,
  ParseIntPipe,
  NotFoundException,
} from '@nestjs/common';
import { OrganisationService } from './organisation.service';
import { UserService } from '../user/user.service';

@Controller('users')
export class UserController {
  constructor(
    private readonly service: OrganisationService,
    private readonly userService: UserService,
  ) {}

  @Get('/:id/organisations')
  async getOrganisations(@Param('id', ParseIntPipe) id: number) {
    const user = await this.userService.getUserById(id);
    if (!user) {
      throw new NotFoundException();
    }
    return this.service.getManyForUser(user);
  }
}
