import {
  Controller,
  Post,
  Body,
  Patch,
  BadRequestException,
  UsePipes,
  ValidationPipe,
  Delete,
  Param,
  ParseIntPipe,
  Get,
  NotFoundException,
  Request,
} from '@nestjs/common';
import { OrganisationService } from './organisation.service';
import { CreateOrganisationDTO } from './dto/createOrganisation.dto';
import { UpdateOrganisationDTO } from './dto/updateOrganisation.dto';
import { objectIsEmpty } from '../common/util';
import { AddMemberDTO } from './dto/addMember.dto';

@UsePipes(
  new ValidationPipe({
    disableErrorMessages: false,
    whitelist: true,
  }),
)
@Controller('organisations')
export class OrganisationController {
  constructor(public service: OrganisationService) {}

  @Get()
  async getMany(): Promise<any> {
    return this.service.getMany();
  }

  @Get('/:id')
  async getOne(@Param('id', ParseIntPipe) orgId: number): Promise<any> {
    const user = await this.service.getOne(orgId);
    if (!user) {
      throw new NotFoundException();
    }
    return user;
  }

  @Get('/:id/members')
  async getMembers(@Param('id', ParseIntPipe) orgId: number) {
    return this.service.getMembers(orgId);
  }

  @Get('/:orgId/members/:memberId')
  async getOneMember(
    @Param('orgId', ParseIntPipe) orgId: number,
    @Param('memberId', ParseIntPipe) memberId: number,
  ) {
    const member = await this.service.getOneMember(orgId, memberId);

    // Throw 404 if not found
    if (member === null) {
      throw new NotFoundException();
    }

    return member;
  }

  @Delete('/:orgId/members/:memberId')
  async deleteOneMember(
    @Param('orgId', ParseIntPipe) orgId: number,
    @Param('memberId', ParseIntPipe) memberId: number,
  ) {
    return this.service.deleteOneMember(orgId, memberId);
  }

  @Post('/:orgId/members')
  async addMembers(
    @Param('orgId', ParseIntPipe) orgId: number,
    @Body() dto: AddMemberDTO,
  ) {
    // Atleast userId or userIds must be filled in
    if (objectIsEmpty(dto)) {
      throw new BadRequestException();
    }

    // Add a single user
    if (typeof dto.userId === 'number') {
      return this.service.addOneMember(orgId, dto.userId);
    }

    // Add multiple users
    if (Array.isArray(dto.userIds)) {
      return this.service.addManyMembers(orgId, dto.userIds);
    }
  }

  @Post()
  async createOne(
    @Request() req,
    @Body() dto: CreateOrganisationDTO,
  ): Promise<any> {
    return await this.service.createOne(dto, req.user);
  }

  @Patch('/:id')
  async updateOne(
    @Param('id', ParseIntPipe) orgId: number,
    @Body() dto: UpdateOrganisationDTO,
  ): Promise<any> {
    if (objectIsEmpty(dto)) {
      throw new BadRequestException();
    }
    return this.service.updateOne(orgId, dto);
  }

  @Delete('/:id')
  async deleteOne(@Param('id', ParseIntPipe) orgId: number): Promise<any> {
    return this.service.deleteOne(orgId);
  }
}
