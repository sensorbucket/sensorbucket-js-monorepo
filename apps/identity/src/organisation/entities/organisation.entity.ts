import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  ManyToMany,
  JoinTable,
} from 'typeorm';
import { Organisation as OrganisationModel } from '@sensorbucket/models';
import { User } from '../../user/entities/user.entity';

@Entity()
export class Organisation implements OrganisationModel {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    length: '75',
  })
  name: string;

  @Column({
    length: '75',
  })
  address: string;

  @Column({
    length: '6',
  })
  zipcode: string;

  @Column({
    length: '75',
  })
  city: string;

  @Column('geometry', {
    spatialFeatureType: 'Point',
    srid: 4326,
  })
  geom: object;

  @ManyToMany((type) => User, (user) => user.organisations)
  @JoinTable({
    name: 'organisation_members',
  })
  members: User[];
}
