import {
  Injectable,
  BadRequestException,
  NotFoundException,
} from '@nestjs/common';
import { Organisation } from './entities/organisation.entity';
import { Repository, getConnection } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { CreateOrganisationDTO } from './dto/createOrganisation.dto';
import { UpdateOrganisationDTO } from './dto/updateOrganisation.dto';
import { objectIsEmpty } from '../common/util';
import { UserService } from '../user/user.service';
import { User } from '../user/entities/user.entity';

@Injectable()
export class OrganisationService {
  constructor(
    @InjectRepository(Organisation)
    private readonly repository: Repository<Organisation>,
    private readonly userService: UserService,
  ) {}

  /**
   * Get an organisation by its ID
   * @param orgId The id of the organisation
   */
  public getOne(orgId: number): Promise<Organisation | undefined> {
    return this.repository.findOne(orgId);
  }

  /**
   * getMany
   */
  public getMany(): Promise<Organisation[]> {
    return this.repository.find();
  }

  /**
   * Get members of a specific organisation
   */
  public async getMembers(orgId: number): Promise<User[]> {
    const organisationExists =
      (await this.repository.findOne(orgId)) !== undefined;

    if (!organisationExists) {
      throw new NotFoundException();
    }
    return getConnection()
      .createQueryBuilder()
      .relation(Organisation, 'members')
      .of(orgId)
      .loadMany();
  }

  /**
   * Get a specific member
   * @param orgId The organisation id
   * @param memberId The userId of the member
   */
  public async getOneMember(orgId: number, memberId: number): Promise<User> {
    const member = await getConnection()
      .createQueryBuilder(User, 'user')
      .innerJoin('user.organisations', 'org')
      .where('org.id = :orgId', { orgId })
      .andWhere('user.id = :memberId', { memberId })
      .getOne();
    return member || null;
  }

  /**
   * Add a user to an organisation
   * @param orgId
   * @param inviterId
   * @param inviteeId
   */
  public async addOneMember(orgId: number, inviteeId: number): Promise<any> {
    const members = await this.getMembers(orgId);

    // Check if the invitee is already part of the org
    const isInviteeMember =
      members.find((member) => member.id === inviteeId) !== undefined;
    if (isInviteeMember) {
      return new BadRequestException(
        'User is already a member of the organisation',
      );
    }

    // User must exist
    const user = await this.userService.getUserById(inviteeId);
    if (!user) {
      throw new BadRequestException('No user found with that id');
    }

    // Add the user
    await this.repository
      .createQueryBuilder()
      .relation('members')
      .of(orgId)
      .add(inviteeId);

    return {
      success: true,
      message: 'The user was added to the organisation',
    };
  }

  /**
   * Add a user to an organisation
   * @param orgId
   * @param inviterId
   * @param inviteeId
   */
  public async addManyMembers(
    orgId: number,
    inviteeIds: number[],
  ): Promise<any> {
    const members = await this.getMembers(orgId);
    const memberIds = members.map((member) => member.id);

    // Filter out invitees that are already members,
    // keeping non-members
    const filteredInviteeIds = inviteeIds.filter(
      (id) => !memberIds.includes(id),
    );

    // Filter out users that don't exist
    const users = await Promise.all(
      filteredInviteeIds.map((id) => this.userService.getUserById(id)),
    );
    const toInviteIds = users.filter((u) => !!u).map((u) => u.id);

    if (toInviteIds.length === 0) {
      throw new BadRequestException('No users to add to organisation');
    }

    // Add the user
    await this.repository
      .createQueryBuilder()
      .relation('members')
      .of(orgId)
      .add(toInviteIds);

    return {
      success: true,
      message: 'Users were added to the organisation',
      data: toInviteIds,
    };
  }

  /**
   * Remove a user from the organisation
   * @param orgId The organisation id
   * @param memberId The userId of the member
   */
  public async deleteOneMember(orgId: number, memberId: number): Promise<any> {
    const members = await this.getMembers(orgId);

    // Check if this is a member
    const isMember = members.find((user) => user.id === memberId) !== undefined;
    if (!isMember) {
      throw new NotFoundException('User is not part of the given organisation');
    }

    // If this is the last user, remove the organisation
    const isLastMember = members.length === 1;
    if (isLastMember) {
      await this.deleteOne(orgId);
      return {
        success: true,
        message: 'Organisation is removed because it had no members left',
      };
    }

    // Remove member from organisation
    return getConnection()
      .createQueryBuilder()
      .relation(Organisation, 'members')
      .of(orgId)
      .remove(memberId);
  }

  /**
   * Get the organisations a user belongs to
   * @param userId The user
   */
  public async getManyForUser(user: User): Promise<Organisation[]> {
    return getConnection()
      .createQueryBuilder()
      .relation(User, 'organisations')
      .of(user.id)
      .loadMany();
  }

  /**
   * Create a new organisation
   */
  public async createOne(
    dto: CreateOrganisationDTO,
    userId: number,
  ): Promise<Organisation> {
    const orgLike = this.repository.create(dto);

    // Resolve user and add it to the members list
    const user = await this.userService.getUserById(userId);
    orgLike.members = [user];

    const org = await this.repository.save(orgLike);
    return org;
  }

  /**
   * updateOne
   */
  public async updateOne(
    orgId: number,
    dto: UpdateOrganisationDTO,
  ): Promise<Organisation> {
    if (objectIsEmpty(dto)) {
      throw new BadRequestException();
    }
    const current = await this.repository.findOne(orgId);
    const patched = {
      ...current,
      ...dto,
    };
    return this.repository.save(patched);
  }

  /**
   * deleteOne
   */
  public async deleteOne(orgId: number): Promise<any> {
    const org = await this.repository.findOne(orgId);
    if (!org) {
      throw new NotFoundException();
    }
    await this.repository.remove(org);
    return {
      success: true,
      message: 'Sucessfully deleted organisation',
    };
  }
}
