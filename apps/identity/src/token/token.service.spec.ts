import { Test, TestingModule } from '@nestjs/testing';
import { TokenService } from './token.service';
import { getRepositoryToken } from '@nestjs/typeorm';
import { RevokedToken } from './entities/revokedToken.entity';
import { JwtService } from '@nestjs/jwt';
import { plainToClass } from 'class-transformer';
import { ConfigModule } from '../config/config.module';

const RevokedTokenRepository = {
  count: jest.fn(),
  create: jest.fn(dto => plainToClass(RevokedToken, dto)),
  save: jest.fn(),
};

const JWTServiceMock = {
  sign: jest.fn(),
  verify: jest.fn(),
};

describe('TokenService', () => {
  let service: TokenService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [ConfigModule],
      providers: [
        TokenService,
        {
          provide: JwtService,
          useValue: JWTServiceMock,
        },
        {
          provide: getRepositoryToken(RevokedToken),
          useValue: RevokedTokenRepository,
        },
      ],
    }).compile();

    service = module.get<TokenService>(TokenService);
  });

  afterEach(() => {
    RevokedTokenRepository.save.mockClear();
    RevokedTokenRepository.count.mockClear();
    RevokedTokenRepository.create.mockClear();
    JWTServiceMock.sign.mockClear();
  });

  it('should return false if token is not in DB', async () => {
    const aud = 'testingtoken';
    const jti = 'testjti';
    RevokedTokenRepository.count.mockReturnValue(0);
    //
    const blacklisted = await service.isRevoked(jti, aud);
    //
    expect(blacklisted).toEqual(false);
  });

  it('should return true if token is in DB', async () => {
    const aud = 'testingtoken';
    const jti = 'testjti';
    RevokedTokenRepository.count.mockReturnValue(1);
    //
    const blacklisted = await service.isRevoked(jti, aud);
    //
    expect(blacklisted).toEqual(true);
  });

  it('should revoke token', async () => {
    const token = 'testtoken';
    const jti = 'testjti';
    const aud = 'users';
    RevokedTokenRepository.count.mockReturnValue(0);
    JWTServiceMock.verify.mockReturnValue({ jti, aud });
    //
    await service.revoke(token);
    //
    expect(JWTServiceMock.verify).toBeCalled();
    expect(RevokedTokenRepository.create).toBeCalled();
    expect(RevokedTokenRepository.save).toBeCalled();
  });

  it('should revoke by JTI and audience', async () => {
    const jti = 'testjti';
    const aud = 'users';
    RevokedTokenRepository.count.mockReturnValue(0);
    //
    await service.revoke(jti, aud);
    //
    expect(RevokedTokenRepository.create).toBeCalled();
    expect(RevokedTokenRepository.save).toBeCalled();
  });
});
