import { Module } from '@nestjs/common';
import { TokenService } from './token.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { RevokedToken } from './entities/revokedToken.entity';
import { KeyModule } from '@sensorbucket/common';
import { AppConfig } from '../config/config.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([RevokedToken]),
    KeyModule.forRootAsync({
      inject: [AppConfig],
      useFactory: (config: AppConfig) => ({
        kmsURL: config.kmsURL,
      }),
    }),
  ],
  providers: [TokenService],
  exports: [TokenService],
})
export class TokenModule {}
