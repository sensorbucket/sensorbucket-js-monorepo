import {
  Injectable,
  BadRequestException,
  InternalServerErrorException,
  Logger,
} from '@nestjs/common';
import { RevokedToken } from './entities/revokedToken.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { AppConfig } from '../config/config.service';
import * as JWT from 'jsonwebtoken';
import Axios from 'axios';
import { KeyService } from '@sensorbucket/common';

@Injectable()
export class TokenService {
  private readonly logger = new Logger(TokenService.name, true);
  private readonly config: AppConfig;
  private readonly keys: KeyService;
  private readonly repo: Repository<RevokedToken>;
  private keyCache = { ttl: 0, key: null, kid: null };

  constructor(
    config: AppConfig,
    keys: KeyService,
    @InjectRepository(RevokedToken) repo: Repository<RevokedToken>,
  ) {
    this.config = config;
    this.keys = keys;
    this.repo = repo;
  }

  /**
   * Fires when the module is initializing
   */
  async onModuleInit() {
    try {
      // Assert keystore existance
      const created = await this.keys.assertKeystore(this.config.kmsKeystore);
      if (created) {
        this.logger.log(
          `Keystore ${this.config.kmsKeystore} did not exist, created it through assertion.`,
        );
      }
    } catch (e) {
      this.logger.error('Fatal error: ', e.toString());
      process.exit();
    }
  }

  /**
   * Revokes a token
   * The token must have a JTI and Audience set and optionally an expiry date
   * @param token The token to be revoked
   */
  revoke(token: string): Promise<void>;

  /**
   * Inserts the given token into the revoked token list
   * @param jti The token its unique identifier
   * @param audience The token its audience
   * @param expiry The expiry date of the token if set
   */
  revoke(jti: string, audience: string, expiry?: Date | null): Promise<void>;

  async revoke(
    jtiOrToken: string,
    audience?: string,
    expiry: Date | null = null,
  ): Promise<void> {
    let jti = jtiOrToken;
    // Overload functionality
    if (typeof audience === 'undefined') {
      const token = JWT.decode(jtiOrToken, { complete: true });
      if (typeof token === 'string') {
        throw new Error('invalid JWT');
      }

      jti = token.jti;
      audience = token.audience;
      expiry = token.expiry;
    }
    // Don't do anything if token is already revoked
    if (await this.isRevoked(jti, audience)) {
      return;
    }

    // Insert new revoked token
    const revokedLike = this.repo.create({
      identification: jti,
      audience,
      expiry,
    });
    await this.repo.save(revokedLike);
  }

  /**
   * Check if a token is blacklisted or not
   * @param token The token to checked against the blacklist
   */
  async isRevoked(jti: string, aud: string): Promise<boolean> {
    const count = await this.repo.count({ identification: jti, audience: aud });
    return count !== 0;
  }

  /**
   *
   * @param uid
   * @param orgIds
   */
  async createUserToken(uid: number, orgIds: number[]): Promise<string> {
    const payload = {
      orgIds,
    };
    const opts = <JWT.SignOptions>{
      algorithm: 'RS256',
      audience: 'users',
      issuer: 'sensorbucket',
      expiresIn: '1h',
      subject: uid.toString(),
    };

    return this.signToken(payload, opts);
  }

  /**
   *
   * @param payload
   * @param options
   */
  private async signToken(
    payload: object,
    options: JWT.SignOptions,
  ): Promise<string> {
    return new Promise(async (resolve, reject) => {
      // Retrieve key at index 0
      const keystore = this.config.kmsKeystore;
      const kid = (await this.keys.getKeystore(keystore)).keys[0].kid;
      const key = await this.keys.getPEM(keystore, kid, true);
      options.keyid = kid;

      // Create and sign token
      JWT.sign(payload, key, options, (err, token) => {
        if (err) {
          return reject(err);
        }
        resolve(token);
      });
    });
  }
}
