import {
  Entity,
  Column,
  CreateDateColumn,
  PrimaryGeneratedColumn,
  Unique,
} from 'typeorm';

@Entity()
@Unique('tokenID', ['identification', 'audience'])
export class RevokedToken {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  identification: string;

  @Column()
  audience: string;

  @CreateDateColumn()
  created_at: string;

  @Column({
    nullable: true,
  })
  expiry: Date;
}
