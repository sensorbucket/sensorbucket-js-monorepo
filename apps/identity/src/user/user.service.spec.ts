import { Test, TestingModule } from '@nestjs/testing';
import { UserService } from './user.service';
import { getRepositoryToken } from '@nestjs/typeorm';
import { User } from './entities/user.entity';
import { CreateUserDTO } from './dto/createUser.dto';
import * as bcrypt from 'bcrypt';
import { Repository } from 'typeorm';
import { plainToClass } from 'class-transformer';
import { BadRequestException } from '@nestjs/common';
import { ConfigModule } from '../config/config.module';

const repo = new Repository<User>();
const RepoMock = {
  count: jest.fn(),
  create: jest.fn(dto => plainToClass(User, dto)),
  save: jest.fn(user => {
    user.id = 100;
    return user;
  }),
};

describe('AuthenticationService', () => {
  let service: UserService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [ConfigModule],
      providers: [
        // Mock JWTService
        {
          provide: getRepositoryToken(User),
          useValue: RepoMock,
        },
        UserService,
      ],
    }).compile();

    service = module.get<UserService>(UserService);
  });

  afterEach(() => {
    RepoMock.count.mockClear();
    RepoMock.save.mockClear();
  });

  it('#registerUser should create user with hashed password', async () => {
    // Arrange
    const dto: CreateUserDTO = {
      email: 'info@sensorbucket.nl',
      firstName: 'John',
      middleName: undefined,
      lastName: 'Doe',
      password: 'difficultpassword',
    };

    // Act
    await service.registerUser(dto);

    // Assert
    expect(RepoMock.count).toBeCalled();
    expect(RepoMock.save).toBeCalled();
    const inserted = RepoMock.save.mock.calls[0][0] as User;
    expect(inserted).toBeInstanceOf(User);
    // Expect password to match hash
    expect(
      bcrypt.compareSync(dto.password, inserted.passwordHash),
    ).toBeTruthy();
  });

  it('#registerUser should throw on duplicate email', async () => {
    // Arrange
    const dto: CreateUserDTO = {
      email: 'info@sensorbucket.nl',
      firstName: 'John',
      middleName: undefined,
      lastName: 'Doe',
      password: 'difficultpassword',
    };
    RepoMock.count.mockImplementation(() => 1);

    // Act
    const promise = service.registerUser(dto);

    // Assert
    await expect(promise).rejects.toBeInstanceOf(BadRequestException);
  });
});
