import {
  Controller,
  Post,
  Body,
  UsePipes,
  ValidationPipe,
  UseGuards,
  Get,
  Param,
  Patch,
  ParseIntPipe,
  NotFoundException,
} from '@nestjs/common';
import { CreateUserDTO } from './dto/createUser.dto';
import { UserService } from './user.service';
import { User } from './entities/user.entity';
import { PatchUserDTO } from './dto/patchUser.dto';
import { AuthGuard } from '@nestjs/passport';
import { OnlySelfGuard } from '../common/onlySelf.guard';

@UsePipes(new ValidationPipe({ whitelist: true }))
@Controller('users')
export class UserController {
  constructor(private readonly service: UserService) {}

  /**
   * Register a new user
   * @param dto
   */
  @Post()
  registerUser(@Body() dto: CreateUserDTO): Promise<User> {
    return this.service.registerUser(dto);
  }

  /**
   *
   */
  @UseGuards(AuthGuard('sensorbucket-jwt'))
  @Get(':id')
  async getUser(@Param('id', ParseIntPipe) userId: number): Promise<any> {
    const user = await this.service.getUserById(userId);
    if (!user) {
      throw new NotFoundException();
    }

    return user;
  }

  /**
   *
   */
  @Patch(':id')
  @UseGuards(
    AuthGuard('sensorbucket-jwt'),
    OnlySelfGuard({ parameterName: 'id' }),
  )
  async updateUser(
    @Param('id', ParseIntPipe) userId: number,
    @Body() dto: PatchUserDTO,
  ): Promise<any> {
    // Use most up to date user as base and overwrite with DTO
    const user = await this.service.getUserById(userId);
    const properties = {
      ...user,
      ...dto,
    };

    // Update the user
    await this.service.updateUser(userId, properties);
    return {
      success: true,
      message: 'User has been updated',
    };
  }
}
