import {
  IsString,
  IsOptional,
  IsNotEmpty,
  IsNotEmptyObject,
} from 'class-validator';

export class PatchUserDTO {
  @IsString()
  @IsOptional()
  firstName: string;

  @IsString()
  @IsOptional()
  middleName: string;

  @IsString()
  @IsOptional()
  lastName: string;
}
