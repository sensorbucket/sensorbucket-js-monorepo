import { Injectable, BadRequestException } from '@nestjs/common';
import { CreateUserDTO } from './dto/createUser.dto';
import { User } from './entities/user.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import * as bcrypt from 'bcrypt';

@Injectable()
export class UserService {
  constructor(
    @InjectRepository(User) private readonly repository: Repository<User>,
  ) {}

  /**
   * Generate a hash from a plain text password
   * @param password The plain text password
   */
  private async hashPassword(password: string): Promise<string> {
    return await bcrypt.hash(password, 10);
  }

  /**
   * Registers a new user
   * @param dto DTO
   */
  async registerUser(dto: CreateUserDTO): Promise<User> {
    // Check if a record with this email already exists
    const count = await this.repository.count({ email: dto.email });
    if (count > 0) {
      throw new BadRequestException('Email already in use');
    }

    // Hash password
    const passwordHash = await this.hashPassword(dto.password);

    // Create and save user
    const user = this.repository.create(dto);
    user.passwordHash = passwordHash;
    await this.repository.save(user);

    return user;
  }

  /**
   * Update a user its information
   * @param userId The id of the user to update
   * @param updated The updated properties of the user
   */
  async updateUser(userId: number, updated: any): Promise<boolean> {
    await this.repository.update(userId, updated);
    return true;
  }

  /**
   * Find a user by its email
   * @param email The email of the user
   */
  async getUserByEmail(email: string, extra: any): Promise<User> {
    const builder = this.repository
      .createQueryBuilder('user')
      .where('user.email = :email', { email });
    if (Array.isArray(extra.addSelect)) {
      builder.addSelect(extra.addSelect);
    }
    return builder.getOne();
  }

  /**
   * Find a user by its id
   * @param id The id of the user
   */
  getUserById(id: number): Promise<User> {
    return this.repository.findOne(id);
  }
}
