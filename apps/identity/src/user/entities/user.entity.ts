import { Entity, PrimaryGeneratedColumn, Column, ManyToMany } from 'typeorm';
import { User as UserModel } from '@sensorbucket/models';
import { Organisation } from '../../organisation/entities/organisation.entity';

@Entity()
export class User implements UserModel {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ unique: true })
  email: string;

  @Column()
  firstName: string;

  @Column({ default: '' })
  middleName: string;

  @Column()
  lastName: string;

  @Column({
    select: false,
  })
  passwordHash: string;

  @ManyToMany((type) => Organisation, (org) => org.members)
  organisations: Organisation[];
}
