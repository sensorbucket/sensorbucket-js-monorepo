import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ConfigModule } from './config/config.module';
import { KeyService } from './key.service';
import { KeyController } from './key.controller';
import { AppConfig } from './config/config.service';
import { EncryptedKeyStore } from './entities/key.entity';

@Module({
  imports: [
    ConfigModule,
    TypeOrmModule.forRootAsync({
      imports: [],
      inject: [AppConfig],
      useFactory: (config: AppConfig) => ({
        type: 'postgres',
        host: config.dbHost,
        port: config.dbPort,
        username: config.dbUsername,
        password: config.dbPassword,
        database: config.dbName,
        synchronize: true,
        autoLoadEntities: true,
      }),
    }),
    TypeOrmModule.forFeature([EncryptedKeyStore]),
  ],
  controllers: [KeyController],
  providers: [KeyService],
  exports: [],
})
export class KeyModule {}
