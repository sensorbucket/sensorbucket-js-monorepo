import { Injectable, Logger } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { MissingConfigurationError } from '../errors/missingConfiguration.error';

@Injectable()
export class AppConfig {
  constructor(private readonly config: ConfigService) {}

  //
  //  Database configuration
  //

  get dbHost() {
    return this.config.get('DB_HOST', '127.0.0.1');
  }

  get dbPort() {
    return parseInt(this.config.get<string>('DB_PORT', '5432'));
  }

  get dbUsername() {
    return this.config.get('DB_USERNAME', 'root');
  }

  get dbPassword() {
    return this.config.get('DB_PASSWORD', 'root');
  }

  get dbName() {
    return this.config.get('DB_NAME', 'sensorbucket');
  }

  //
  // Key settings
  //

  get aesKey() {
    const value = this.config.get('AES_KEY', null);

    // Assert that value is set
    if (value === null) {
      throw new MissingConfigurationError('AES_KEY');
    }

    return value;
  }
}
