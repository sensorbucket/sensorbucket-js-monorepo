export class MissingConfigurationError extends Error {
  constructor(property: string) {
    super(
      `The configuration property '${property}' is not set but is required.`,
    );
  }
}
