import { Entity, PrimaryGeneratedColumn, Column, PrimaryColumn } from 'typeorm';

@Entity()
export class EncryptedKeyStore {
  @PrimaryColumn()
  id: string;

  // Base64 encoded AES-256 encrypted JWKS
  @Column()
  keystore: string;

  // Base64 encoded Auth tags
  @Column()
  authTag: string;

  // Base64 encoded IV
  @Column()
  iv: string;
}
