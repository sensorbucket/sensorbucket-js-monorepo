import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { AuthenticationMiddleware } from './common/middleware/auth.middleware';
import { APIErrorFilter } from './common/filters/apiError.filter';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.use(AuthenticationMiddleware);
  app.useGlobalFilters(new APIErrorFilter());
  await app.listen(3000);
}
bootstrap();
