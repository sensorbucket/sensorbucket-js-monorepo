import { Test, TestingModule } from '@nestjs/testing';
import { UplinkService } from './uplink.service';

describe('UplinkService', () => {
  let service: UplinkService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [UplinkService],
    }).compile();

    service = module.get<UplinkService>(UplinkService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
