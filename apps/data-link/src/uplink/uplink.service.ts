import {
  Injectable,
  OnModuleInit,
  BeforeApplicationShutdown,
  BadRequestException,
  Logger,
} from '@nestjs/common';
import { v4 as uuid } from 'uuid';
import * as amqp from 'amqplib';
import { RawMessage } from '@sensorbucket/models';
import { ManagementSyncService } from '../management-sync/management-sync.service';
import { AppConfig } from '../config/config.service';

@Injectable()
export class UplinkService implements OnModuleInit, BeforeApplicationShutdown {
  private readonly logger = new Logger(UplinkService.name, true);
  private readonly management: ManagementSyncService;
  private readonly config: AppConfig;
  private amqpConnection: amqp.Connection;
  private amqpChannel: amqp.Channel;

  constructor(management: ManagementSyncService, config: AppConfig) {
    this.management = management;
    this.config = config;
  }

  async onModuleInit() {
    const {
      mqHost,
      mqVHost,
      mqPort,
      mqUsername,
      mqPassword,
      mqExchange: mqExchangeName,
    } = this.config;

    // Setup connections
    try {
      this.amqpConnection = await amqp.connect({
        hostname: mqHost,
        vhost: mqVHost,
        port: mqPort,
        username: mqUsername,
        password: mqPassword,
      });

      const channel = await this.amqpConnection.createChannel();
      this.amqpChannel = channel;

      // Setup exchange
      const exchange = await channel.assertExchange(mqExchangeName, 'topic', {
        autoDelete: false,
        durable: true,
      });
    } catch (e) {
      this.logger.error('Error in Message-Queue setup: ', e);
      throw e;
    }
  }

  async beforeApplicationShutdown(_signal?: string) {
    await this.amqpConnection.close();
  }

  /**
   * Creates a blank raw message with id set
   */
  createUplinkMessage(
    source: string,
    deviceType: string,
    owner: number,
    payload: any,
  ) {
    return <RawMessage>{
      id: uuid(),
      direction: 'uplink',
      dateTime: new Date().toISOString(),
      source,
      deviceType,
      owner,
      payload,
    };
  }

  /**
   * Sends the message to the message queue for it to be processed
   * @param message The message to queue
   */
  async queueRawMessage(message: RawMessage) {
    // Make sure the source and DeviceType exists
    const source = this.management.getSource(message.source);
    if (source === null) {
      throw new BadRequestException(
        `Source '${message.source}' does not exist`,
      );
    }
    const deviceType = this.management.getDeviceType(message.deviceType);
    if (deviceType === null) {
      throw new BadRequestException(
        `Device '${message.deviceType}' Type does not exist`,
      );
    }

    // Create the routing key and publish the message
    const routingkey = `uplink.source.${message.source}`;
    this.amqpChannel.publish(
      this.config.mqExchange,
      routingkey,
      Buffer.from(JSON.stringify(message)),
    );

    return {
      success: true,
      message: 'Uplink message has been accepted',
      data: { uuid: message.id },
    };
  }
}
