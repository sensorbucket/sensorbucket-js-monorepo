import { Module } from '@nestjs/common';
import { UplinkService } from './uplink.service';
import { UplinkController } from './uplink.controller';
import { ManagementSyncModule } from '../management-sync/management-sync.module';

@Module({
  imports: [ManagementSyncModule],
  providers: [UplinkService],
  controllers: [UplinkController],
})
export class UplinkModule {}
