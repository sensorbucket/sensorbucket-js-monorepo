import {
  Controller,
  Post,
  Param,
  Request as GetRequest,
  Get,
} from '@nestjs/common';
import { UplinkService } from './uplink.service';
import { ManagementSyncService } from '../management-sync/management-sync.service';
import { Request } from 'express';

@Controller('uplink')
export class UplinkController {
  service: UplinkService;
  constructor(
    service: UplinkService,
    private readonly managementService: ManagementSyncService,
  ) {
    this.service = service;
  }

  @Post('/:source/:device')
  async CreateRawUplinkMessage(
    @Param('source') sourceId: string,
    @Param('device') deviceId: string,
    @GetRequest() req: Request,
  ) {
    const payload = {
      http: {
        body: req.body,
        headers: req.headers,
      },
    };

    const message = this.service.createUplinkMessage(
      sourceId,
      deviceId,
      req.user.org,
      payload,
    );

    return await this.service.queueRawMessage(message);
  }

  @Get('/sync')
  synchronize() {
    return this.managementService.synchronize();
  }
}
