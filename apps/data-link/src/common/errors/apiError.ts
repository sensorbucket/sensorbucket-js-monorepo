export class APIError extends Error {
  httpStatus: number = 500;
  name: string = "UNKNOWN_API_ERROR";
  constructor(message?: string) {
    super(message);
  }
}
