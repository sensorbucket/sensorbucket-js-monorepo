import * as jwt from 'jsonwebtoken';
import { MalformedTokenError } from '../errors/malformedToken.error';
import { Request } from 'express';

declare module 'express' {
  interface Request {
    user: any;
  }
}

export const AuthenticationMiddleware = (
  req: Request,
  res: any,
  next: () => void,
) => {
  const header = <string>req.headers['authorization'] || null;

  // No auth header
  if (header === null) {
    req.user = null;
    return next();
  }

  const parts = header.split(' ');

  // Format only has two parts "Bearer <token>"
  if (parts.length != 2) {
    throw new MalformedTokenError();
  }

  // Only support bearer tokens
  if (parts[0].toLowerCase() !== 'bearer') {
    throw new MalformedTokenError(
      'Authentication only supports bearer tokens.',
    );
  }

  let payload = null;
  try {
    payload = jwt.decode(parts[1]) as Record<string, any>;
  } catch (e) {
    throw new MalformedTokenError();
  }

  req.user = payload;

  next();
};
