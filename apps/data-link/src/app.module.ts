import { Module } from '@nestjs/common';
import { UplinkModule } from './uplink/uplink.module';
import { ManagementSyncModule } from './management-sync/management-sync.module';
import { ConfigModule } from './config/config.module';

@Module({
  imports: [ConfigModule, UplinkModule, ManagementSyncModule],
  controllers: [],
  providers: [],
})
export class AppModule {}
