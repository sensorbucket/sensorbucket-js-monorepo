import { Test, TestingModule } from '@nestjs/testing';
import { ManagementSyncService } from './management-sync.service';

describe('ManagementSyncService', () => {
  let service: ManagementSyncService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [ManagementSyncService],
    }).compile();

    service = module.get<ManagementSyncService>(ManagementSyncService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
