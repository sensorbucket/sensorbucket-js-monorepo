import { Module } from '@nestjs/common';
import { ManagementSyncService } from './management-sync.service';

@Module({
  providers: [ManagementSyncService],
  exports: [ManagementSyncService],
})
export class ManagementSyncModule {}
