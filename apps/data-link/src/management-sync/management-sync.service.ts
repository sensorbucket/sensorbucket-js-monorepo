import {
  Injectable,
  OnApplicationBootstrap,
  OnApplicationShutdown,
  Logger,
} from '@nestjs/common';
import { DeviceType, Source } from '@sensorbucket/models';
import axios from 'axios';
import { AppConfig } from '../config/config.service';
/**
 * The ManagementSynchronizationService can synchronize existing sources and
 * device-types from the device-management service.
 */
@Injectable()
export class ManagementSyncService
  implements OnApplicationBootstrap, OnApplicationShutdown {
  private readonly logger = new Logger(ManagementSyncService.name, true);
  private readonly config: AppConfig;

  private deviceTypes: Record<string, DeviceType> = {};
  private sources: Record<string, Source> = {};
  private syncTimeout: NodeJS.Timeout;

  constructor(config: AppConfig) {
    this.config = config;
  }

  /**
   * Starts synchronizing at an interval on application bootstrap
   */
  onApplicationBootstrap() {
    this.syncTimeout = setInterval(
      this.synchronize.bind(this),
      this.config.syncInterval * 1000,
    );
    this.synchronize();
  }

  /**
   * Stop synchronization interval on application shutdown
   */
  onApplicationShutdown() {
    clearInterval(this.syncTimeout);
  }

  /**
   * Synchronizes device-types and sources with the
   * device-management service
   */
  async synchronize() {
    // Create buffer
    const deviceTypes: Record<string, DeviceType> = {};
    const sources: Record<string, Source> = {};

    try {
      const [deviceTypesRequest, sourcesRequest] = await Promise.all([
        axios.get<DeviceType[]>(`${this.config.managementURL}/device-types`),
        axios.get<Source[]>(`${this.config.managementURL}/sources`),
      ]);

      // Convert array to hashmap
      deviceTypesRequest.data.forEach((dt) => (deviceTypes[dt.id] = dt));
      sourcesRequest.data.forEach((s) => (sources[s.id] = s));
    } catch (e) {
      // Axios server errored
      if (e.response) {
        this.logger.error(
          'Device-management service responded with an error, synchronize failed.',
          `Error message: '${e.message}'\nResponse status: ${e.response.status}\nResponse body:\n----\n${e.response.data}\n----`,
        );
      } else if (e.request) {
        // Axios request errored
        this.logger.error(
          'Failed to make request to device-management service, synchronize failed. Is the URL correct?',
          e,
        );
      } else {
        this.logger.error(
          'An error occured during synchronization with device-management service',
          e.stack,
        );
      }
      return;
    }

    // Swap in use w/ buffer
    this.deviceTypes = deviceTypes;
    this.sources = sources;

    this.logger.log(
      `Synchronized ${Object.values(deviceTypes).length} device-types and ${
        Object.values(sources).length
      } sources.`,
    );
  }

  /**
   * Get a device-type by its id
   * @param id
   */
  getDeviceType(id: string): DeviceType {
    return this.deviceTypes[id] || null;
  }

  /**
   * Get a source by its id
   * @param id
   */
  getSource(id: string): Source {
    return this.sources[id] || null;
  }
}
