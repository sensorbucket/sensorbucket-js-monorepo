import { Injectable, Logger } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';

@Injectable()
export class AppConfig {
  constructor(private readonly config: ConfigService) {}

  //
  //  MessageQueue configuration
  //
  //'amqp://sensorbucket:sensorbucket@message-queue-rabbitmq:5672',

  get mqHost() {
    return this.config.get('MQ_HOST', '127.0.0.1');
  }

  get mqVHost() {
    return this.config.get('MQ_VIRTUAL_HOST', '/');
  }

  get mqPort() {
    return parseInt(this.config.get<string>('MQ_PORT', '5672'));
  }

  get mqUsername() {
    return this.config.get('MQ_USERNAME', 'guest');
  }

  get mqPassword() {
    return this.config.get('MQ_PASSWORD', 'guest');
  }

  get mqExchange() {
    return this.config.get('MQ_EXCHANGE', 'datalink');
  }

  //
  // Synchronization configuration
  //

  get managementURL() {
    return this.config.get('SYNC_MANAGEMENT_URL', 'http://sb-management');
  }

  get syncInterval() {
    return parseInt(this.config.get<string>('SYNC_INTERVAL_SECONDS', '60'));
  }
}
