import { Module } from '@nestjs/common';
import { MessageQueueService } from './messageQueue.service';

@Module({
  imports: [],
  providers: [MessageQueueService],
  exports: [MessageQueueService],
})
export class MessageQueueModule {}
