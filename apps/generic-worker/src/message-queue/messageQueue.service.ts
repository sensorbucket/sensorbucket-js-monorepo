import {
  Injectable,
  OnModuleInit,
  Logger,
  OnModuleDestroy,
} from '@nestjs/common';
import * as amqp from 'amqplib';
import { AppConfig } from '../config/config.service';
import { Message } from 'amqplib';
import { RawMessage } from '@sensorbucket/models';

@Injectable()
export class MessageQueueService implements OnModuleInit, OnModuleDestroy {
  private readonly logger = new Logger(MessageQueueService.name, true);
  private readonly config: AppConfig;

  private amqpConnection: amqp.Connection;
  private amqpChannel: amqp.Channel;

  constructor(config: AppConfig) {
    this.config = config;
  }

  /**
   * Fired when the application is starting
   * Creates the MessageQueue connections and prepares the
   * exchanges, queue, binding
   */
  async onModuleInit() {
    const {
      mqHost,
      mqVHost,
      mqPort,
      mqUsername,
      mqPassword,
      mqExchange,
      mqQueue,
    } = this.config;

    // Setup connections
    try {
      this.logger.log('Connecting to the Message Queue...');
      this.amqpConnection = await amqp.connect({
        hostname: mqHost,
        vhost: mqVHost,
        port: mqPort,
        username: mqUsername,
        password: mqPassword,
      });

      const channel = await this.amqpConnection.createChannel();
      this.amqpChannel = channel;

      // Setup exchange
      const exchange = await channel.assertExchange(mqExchange, 'topic', {
        autoDelete: false,
        durable: true,
      });

      // Setup queue
      const queue = await channel.assertQueue(mqQueue, {
        autoDelete: false,
      });
      this.logger.log('Message Queue connection succesful');
    } catch (e) {
      this.logger.error('Error in Message-Queue setup: ', e);
      process.exit(1);
    }
  }

  async bindQueue(routingKey: string) {
    const { mqQueue, mqExchange } = this.config;

    // Bind queue to exchange
    await this.amqpChannel.bindQueue(mqQueue, mqExchange, routingKey);
    this.logger.log(`Added routing key: ${routingKey}`);
  }

  /**
   * Fired when the application is exiting
   * Gracefully closes the MessageQueue connection
   */
  async onModuleDestroy() {
    this.logger.log('Closing message queue connection...');
    await this.amqpConnection.close();
  }

  /**
   * Starts consuming messages from the queue
   */
  async start(callback: (message: Message, ack: () => void) => void) {
    // Create a handler with a wrapper for acknowledging the message
    const handler = (message: Message) => {
      const ackFunc = () => {
        this.amqpChannel.ack(message);
      };
      callback(message, ackFunc);
    };

    // Start consuming messages
    this.amqpChannel.consume(this.config.mqQueue, handler, {
      noAck: false,
    });
  }

  /**
   *
   * @param message
   * @param routingKey
   */
  publishMessage(message: RawMessage, routingKey: string) {
    this.amqpChannel.publish(
      this.config.mqExchange,
      routingKey,
      Buffer.from(JSON.stringify(message)),
    );
  }
}
