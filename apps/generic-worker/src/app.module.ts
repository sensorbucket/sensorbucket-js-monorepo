import { Module, OnModuleInit, Logger } from '@nestjs/common';
import { ConfigModule } from './config/config.module';
import { ETLModule } from './etl/etl.module';
import { MessageQueueModule } from './message-queue/messageQueue.module';
import { ETLService } from './etl/etl.service';
import { MessageQueueService } from './message-queue/messageQueue.service';
import { ETLInterface, RawMessage } from '@sensorbucket/models';
import { Message } from 'amqplib';
import { ETLHelper } from './etl/etlHelper.service';

@Module({
  imports: [ConfigModule, ETLModule, MessageQueueModule],
  controllers: [],
  providers: [],
})
export class AppModule implements OnModuleInit {
  private readonly logger = new Logger(AppModule.name, true);
  private readonly etlService: ETLService;
  private readonly etlHelper: ETLHelper;
  private readonly mqService: MessageQueueService;
  private etl: ETLInterface;

  constructor(
    etlService: ETLService,
    etlHelper: ETLHelper,
    mqService: MessageQueueService,
  ) {
    this.etlService = etlService;
    this.etlHelper = etlHelper;
    this.mqService = mqService;
  }

  /**
   * Fired when this module is initialized
   * only fired after ETLModule and MessageQueueModule have initialized
   * succesfully, because we depend on them.
   *
   * Here we 'glue' the two services together.
   */
  onModuleInit() {
    // Instantiate ETL Worker Script
    const ETL = this.etlService.ETL;
    this.etl = new this.etlService.ETL();

    // Create a logger for the ETL Worker
    const etlLogger = new Logger(ETL.name, true);

    // Initialize the worker
    const capabilities = this.etl.initialize(this.etlHelper, etlLogger);

    /*
      Worker capabilities and routing keys are bound one-on-one
      If a worker is capable to process `uplink.source.thethingsnetwork` messages 
      then the routing key for those messages is the same
     */
    capabilities.forEach((cap) => {
      this.mqService.bindQueue(cap);
    });

    // Start consuming messages
    this.mqService.start(this.onMessage.bind(this));
  }

  /**
   * Fired when a message is received from the message queue
   * @param message the message
   */
  async onMessage(message: Message, ack: () => void) {
    this.logger.log(
      `Received message: ${message.fields.routingKey}, sending to ETL Script...`,
    );
    const rawMessage = JSON.parse(message.content.toString()) as RawMessage;
    try {
      await this.etl.onMessage(rawMessage);
      ack(); // Acknowledge that the messages have been processed
      this.logger.log(`ETL Script finished parsing message`);
    } catch (e) {
      this.logger.error('ETL Script threw an error! ', e);
      throw e;
    }
  }
}
