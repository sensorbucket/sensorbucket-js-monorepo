import { Injectable, OnModuleInit, Logger } from '@nestjs/common';
import { ETLInterface } from '@sensorbucket/models';
import { join } from 'path';
import { AppConfig } from '../config/config.service';

// definition for a constructor
type Constructor<T> = new (...any: any[]) => T;

/**
 * The ETLService loads the ETL Script
 */
@Injectable()
export class ETLService implements OnModuleInit {
  private readonly logger = new Logger(ETLService.name, true);
  private readonly config: AppConfig;
  private etlScriptClass: Constructor<ETLInterface>;

  constructor(config: AppConfig) {
    this.config = config;
  }

  /**
   * Fired when the module initializes
   *
   * Loads the ETL Script file
   */
  async onModuleInit() {
    const { etlPath } = this.config;
    const filePath = join(process.cwd(), etlPath);

    try {
      this.logger.log(`Loading ETL script at '${filePath}' ...`);
      this.etlScriptClass = (await import(filePath)).default;
    } catch (e) {
      this.logger.error('Failed to load ETL script! Does the file exist?', e);
      process.exit(1);
    }
  }

  /**
   * Get the ETL Script constructor
   */
  get ETL() {
    return this.etlScriptClass;
  }
}
