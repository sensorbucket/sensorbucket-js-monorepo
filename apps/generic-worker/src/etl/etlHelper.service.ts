import {
  Device,
  UplinkMessage,
  ETLHelperInterface,
} from '@sensorbucket/models';
import { Injectable, Logger } from '@nestjs/common';
import Axios from 'axios';
import { encode as encodeQuery } from 'querystring';
import { AppConfig } from '../config/config.service';
import { MessageQueueService } from '../message-queue/messageQueue.service';

@Injectable()
export class ETLHelper implements ETLHelperInterface {
  private readonly logger = new Logger(ETLHelper.name, true);
  private readonly config: AppConfig;
  private readonly queue: MessageQueueService;

  constructor(config: AppConfig, queue: MessageQueueService) {
    this.config = config;
    this.queue = queue;
  }

  async publishUplinkDeviceMessage(message: UplinkMessage): Promise<void> {
    this.logger.log(`Publishing uplink message with id ${message.id}`);
    const routingKey = `uplink.device.${message.deviceType}`;
    this.queue.publishMessage(message, routingKey);
  }

  async getDevice(filter: Record<string, string>): Promise<Device> {
    // Ensure a filter is given
    const filters = Object.keys(filter);
    if (!filters.includes('source') && !filters.includes('device')) {
      console.warn(
        'ETL Script tried to get a device without giving a source/device filter, this is not possible!',
      );
      return null;
    }

    // Get device from management service using a filter
    try {
      const managementURL = this.config.managementURL;
      const result = await Axios.get(
        `${managementURL}/devices/?${encodeQuery(filter)}`,
      );

      // Should match exactly 1 device, if not create a warning
      if (result.data.length === 0) {
        console.warn('Worker matched 0 devices!');
        return null;
      } else if (result.data.length > 1) {
        console.warn('Worker matched multiple devices, choosing first result!');
      }

      return result.data[0];
    } catch (e) {
      this.logger.error('Could not get device!: ', e);
    }
  }
}
