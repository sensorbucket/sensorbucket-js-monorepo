import { Module } from '@nestjs/common';
import { ETLService } from './etl.service';
import { ETLHelper } from './etlHelper.service';
import { MessageQueueModule } from '../message-queue/messageQueue.module';

@Module({
  imports: [MessageQueueModule],
  providers: [ETLService, ETLHelper],
  exports: [ETLService, ETLHelper],
})
export class ETLModule {}
