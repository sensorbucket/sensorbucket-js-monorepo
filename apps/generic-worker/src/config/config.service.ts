import { Injectable, Logger } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';

@Injectable()
export class AppConfig {
  constructor(private readonly config: ConfigService) {}

  //
  //  MessageQueue configuration
  //

  get mqHost() {
    return this.config.get('MQ_HOST', '127.0.0.1');
  }

  get mqVHost() {
    return this.config.get('MQ_VIRTUAL_HOST', '/');
  }

  get mqPort() {
    return parseInt(this.config.get<string>('MQ_PORT', '5672'));
  }

  get mqUsername() {
    return this.config.get('MQ_USERNAME', 'guest');
  }

  get mqPassword() {
    return this.config.get('MQ_PASSWORD', 'guest');
  }

  get mqExchange() {
    return this.config.get('MQ_EXCHANGE', 'data-link');
  }

  get mqQueue() {
    return this.config.get('MQ_QUEUE', 'worker/generic');
  }

  //
  //
  //

  get managementURL() {
    return this.config.get('MANAGEMENT_URL', 'http://sb-management');
  }

  //
  // ETL Script configuration
  //

  get etlPath() {
    return this.config.get('ETL_PATH', './worker/worker.js');
  }
}
