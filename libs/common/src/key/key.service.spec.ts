import { Test } from '@nestjs/testing';
import { mocked } from 'ts-jest/utils';
import _Axios from 'axios';
import { KeyService } from './key.service';
import { KeyModule } from './key.module';
import { KeyStoreNotFoundError } from './errors/keyStoreNotFound.error';
import { KeyStoreKeyNotFoundError } from './errors/keyStoreKeyNotFound.error';

const Axios = mocked(_Axios, true);

const baseURL = 'http://kms';

describe('keyService', () => {
  let service: KeyService;

  beforeEach(async () => {
    const module = await Test.createTestingModule({
      imports: [
        KeyModule.forRootAsync({
          useFactory: () => ({
            kmsURL: baseURL,
          }),
        }),
      ],
    }).compile();

    service = module.get<KeyService>(KeyService);
  });

  it('Should set base_url to config', async () => {
    expect(Axios.create.mock.calls[0][0].baseURL).toEqual(baseURL);
  });

  it('Should get keystore', async () => {
    const data = { keys: [] };
    Axios.get.mockReturnValue(Promise.resolve({ data }));

    const keystore = await service.getKeystore('user-jwt');
    expect(Axios.get).toHaveBeenCalledWith('/public/user-jwt');
    expect(keystore).toEqual(data);
  });

  it('Should throw `KeyStoreNotFoundError` if keystore not found', async () => {
    Axios.get.mockRejectedValue({ response: { status: 404 } });
    expect(service.getKeystore('user-jwt')).rejects.toThrowError(
      KeyStoreNotFoundError,
    );
  });

  it('Should throw `KeyStoreKeyNotFoundError` if key not found', async () => {
    Axios.get.mockRejectedValue({ response: { status: 404 } });
    expect(service.getJWK('user-jwt', 'somekid')).rejects.toThrowError(
      KeyStoreKeyNotFoundError,
    );
    expect(service.getPEM('user-jwt', 'somekid')).rejects.toThrowError(
      KeyStoreKeyNotFoundError,
    );
  });
});
