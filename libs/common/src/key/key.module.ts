import {
  Module,
  DynamicModule,
  Provider,
  ModuleMetadata,
} from '@nestjs/common';
import { KeyService } from './key.service';
import { KEY_MODULE_CONFIG_PROVIDER } from './key.constants';

export interface KeyModuleConfig {
  kmsURL: string;
}

export interface KeyModuleConfigAsync extends Pick<ModuleMetadata, 'imports'> {
  useFactory?: (...args: any[]) => Promise<KeyModuleConfig> | KeyModuleConfig;
  inject?: any[];
}

@Module({})
export class KeyModule {
  /**
   *
   * @param opt
   */
  static forRoot(opt: KeyModuleConfig): DynamicModule {
    // Build a provider for the config
    const keyModuleConfigService = <Provider>{
      provide: KEY_MODULE_CONFIG_PROVIDER,
      useValue: opt,
    };

    // Build the module
    return {
      module: KeyModule,
      providers: [KeyService, keyModuleConfigService],
      exports: [KeyService],
    };
  }

  /**
   * Configure the key module asynchronous with Dependency injection
   * @param opt Async options
   */
  static forRootAsync(opt: KeyModuleConfigAsync): DynamicModule {
    // Build a provider for the config
    const keyModuleConfigService = <Provider>{
      provide: KEY_MODULE_CONFIG_PROVIDER,
      inject: opt.inject || [],
      useFactory: opt.useFactory,
    };

    // Build the module
    return {
      module: KeyModule,
      imports: opt.imports || [],
      providers: [keyModuleConfigService, KeyService],
      exports: [KeyService],
    };
  }
}
