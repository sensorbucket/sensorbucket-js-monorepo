import { APIError } from '../../errors/apiError';

export class KeyStoreNotFoundError extends APIError {
  httpStatus = 404;
  name = 'KMS_KEYSTORE_NOT_FOUND';

  constructor(id: string) {
    super(`The requested KeyStore with id ${id} could not be found`);
  }
}
