import { APIError } from '../../errors/apiError';

export class KeyStoreKeyNotFoundError extends APIError {
  httpStatus = 404;
  name = 'KMS_KEYSTORE_KEY_NOT_FOUND';

  constructor(keystoreId: string, kid: string) {
    super(
      `The requested key with kid ${kid} in keystore ${keystoreId} could not be found`,
    );
  }
}
