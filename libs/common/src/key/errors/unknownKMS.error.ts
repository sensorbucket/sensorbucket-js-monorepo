import { APIError } from '../../errors/apiError';

export class UnknownKMSError extends APIError {
  name = 'KMS_UNKNOWN_ERROR';
  prefix = 'The request to the KMS has failed!';
  suffix = '';
  message = this.prefix;

  constructor(message: string, response?: any) {
    super();
    this.message += message;
    if (response) {
      this.suffix = `\n[${response.status} - ${response.request.path}] ${response.data}`;
    }
    this.message = `${this.prefix} ${message} ${this.suffix}`;
  }
}
