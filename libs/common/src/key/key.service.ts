import { Injectable, Inject, Logger } from '@nestjs/common';
import Axios, { AxiosInstance } from 'axios';
import { KeyModuleConfig } from './key.module';
import { KeyStoreNotFoundError } from './errors/keyStoreNotFound.error';
import { KeyStoreKeyNotFoundError } from './errors/keyStoreKeyNotFound.error';
import { KEY_MODULE_CONFIG_PROVIDER } from './key.constants';
import { UnknownKMSError } from './errors';

export interface JWK {
  kid: string;
}

export interface JWKS {
  keys: JWK[];
}

/**
 * The KeyService provides public and private keys from the Key Management Service
 *
 * The KeyService provides utility functions and caching
 */
@Injectable()
export class KeyService {
  private readonly axios: AxiosInstance;
  private readonly logger = new Logger(KeyService.name, true);

  constructor(@Inject(KEY_MODULE_CONFIG_PROVIDER) config: KeyModuleConfig) {
    let { kmsURL } = config;
    if (!kmsURL.startsWith('http')) {
      kmsURL = 'http://' + kmsURL;
      this.logger.warn(
        `KMS_URL is missing protocol. Prepending 'http://', check your configuration`,
      );
    }
    this.axios = Axios.create({
      baseURL: kmsURL,
    });
  }

  /**
   * Get a keystore by its id
   * @param id The keystore id
   * @param priv True to return private keys
   */
  async getKeystore(id: string, priv = false) {
    const prefix = priv ? 'private' : 'public';

    try {
      const result = await this.axios.get<JWKS>(`/${prefix}/${id}`);
      return result.data;
    } catch (e) {
      if (e.response && e.response.status === 404) {
        throw new KeyStoreNotFoundError(id);
      }
      if (e.response) {
        throw new UnknownKMSError(`Could not get keystore ${id}.`, e.response);
      }
      throw e;
    }
  }

  /**
   * Assert that the given keystore exists
   * @param id The keystore ID
   * @returns {boolean} True if the assertion has created the keystore
   */
  async assertKeystore(id: string): Promise<boolean> {
    try {
      await this.getKeystore(id);
      return false;
    } catch (e) {
      // If keystore is not found then create it
      if (e instanceof KeyStoreNotFoundError) {
        await this.createKeystore(id);
        return true;
      } else {
        throw e;
      }
    }
  }

  /**
   * Create a keystore
   * @param id The keystore ID
   */
  private async createKeystore(id: string) {
    try {
      await this.axios.post(`/`, { id });
    } catch (e) {
      if (e.response) {
        throw new UnknownKMSError('Failed to create keystore', e.response);
      }
      throw e;
    }
  }

  /**
   *
   * @param keystoreId The keystore id
   * @param kid The key id
   * @param format The format of the key to return
   * @param priv True to return the private key
   */
  async getKey(
    keystoreId: string,
    kid: string,
    format: 'pem' | 'jwk',
    priv = false,
  ): Promise<string | JWK> {
    const prefix = priv ? 'private' : 'public';

    try {
      const result = await this.axios.get(
        `/${prefix}/${keystoreId}/${kid}?format=${format}`,
      );
      return result.data.data;
    } catch (e) {
      if (e.response && e.response.status === 404) {
        throw new KeyStoreKeyNotFoundError(keystoreId, kid);
      }
      if (e.response) {
        throw new UnknownKMSError('Could not get key', e.response);
      }
      throw e;
    }
  }

  /**
   * Get a key from a keystore as a JSON Web Key
   * @param keystoreId The keystore id
   * @param kid The key id
   * @param priv True to return the private key
   */
  async getJWK(keystoreId: string, kid: string, priv = false): Promise<JWK> {
    return this.getKey(keystoreId, kid, 'jwk', priv) as Promise<JWK>;
  }

  /**
   * Get a key from a keystore as a PEM key
   * @param keystoreId The keystore id
   * @param kid The key id
   * @param priv True to return the private key
   */
  async getPEM(keystoreId: string, kid: string, priv = false): Promise<string> {
    return this.getKey(keystoreId, kid, 'pem', priv) as Promise<string>;
  }
}
