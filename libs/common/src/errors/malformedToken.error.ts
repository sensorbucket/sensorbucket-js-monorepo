import { APIError } from './apiError';
export class MalformedTokenError extends APIError {
  httpStatus = 401;
  name = 'AUTH_MALFORMED_TOKEN';
  message = 'The authentication could not be parsed';
}
