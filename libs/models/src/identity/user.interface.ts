import { Organisation } from './organisation.interface';

export interface User {
  id: number;
  email: string;
  firstName: string;
  middleName: string;
  lastName: string;
  passwordHash: string;
  organisations: Organisation[];
}
