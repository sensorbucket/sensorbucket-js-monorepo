import { User } from './user.interface';

export interface Organisation {
  id: number;
  name: string;
  address: string;
  zipcode: string;
  city: string;
  geom: object;
  members: User[];
}
