import { Device } from '../device-management';
import { UplinkMessage } from '../data-link';

/**
 * An ETLHelper is provided to the ETL Script which can use its functions
 * to access more privileged data.
 */
export interface ETLHelperInterface {
  publishUplinkDeviceMessage(message: UplinkMessage): Promise<void>;
  getDevice(filter: Record<string, any>): Promise<Device>;
}
