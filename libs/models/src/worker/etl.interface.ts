import { Logger } from '@nestjs/common';
import { ETLHelperInterface } from './etlHelper.interface';
import { RawMessage } from '../data-link';

/**
 * Defines the interface that ETL scripts have to fulfill
 */
export interface ETLInterface {
  initialize(helper: ETLHelperInterface, logger: Logger): string[];
  onMessage(message: RawMessage);
}
