import { DeviceType, Source } from '.';

export interface Device {
  id: number;
  name: string;
  owner: number;
  description: string;
  type: DeviceType;
  typeId: string;
  source: Source;
  sourceId: string;
  configuration: {};
}
