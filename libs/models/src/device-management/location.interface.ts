export interface Location {
  id: number;
  owner: number;
  name: string;
  description: string;
  geom: any;
}
