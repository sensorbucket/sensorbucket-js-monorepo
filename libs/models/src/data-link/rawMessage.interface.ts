export interface RawMessage {
  deviceType: string;
  direction: 'uplink' | 'downlink' | 'other';
  id: string;
  owner: number;
  payload: any;
  source: string;
  dateTime: string; // ISO-8601 Time
}
