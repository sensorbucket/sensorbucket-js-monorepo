import { RawMessage } from './rawMessage.interface';

export interface UplinkMessage extends RawMessage {
  deviceData: Buffer;
  device?: number;
}
