require('source-map-support').install();
import { NestFactory } from '@nestjs/core';
import { Module } from '@nestjs/common';
import { AppConfigModule } from './appConfig';

@Module({
  imports: [AppConfigModule],
})
class APIModule {}

async function bootstrap() {
  const app = await NestFactory.create(APIModule);
  await app.listen(3000);
}
bootstrap();
