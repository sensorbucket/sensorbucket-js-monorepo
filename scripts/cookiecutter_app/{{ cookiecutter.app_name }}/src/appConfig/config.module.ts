import { Module, Global } from "@nestjs/common";
import { AppConfig } from "./config.service";
import { ConfigModule } from "@nestjs/config";
@Global()
@Module({
  imports: [ConfigModule],
  providers: [AppConfig],
  exports: [AppConfig],
})
export class AppConfigModule {}
